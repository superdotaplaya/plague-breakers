{
    "id": "6058b07f-930e-4758-9f4d-1b3a2c3a1cf7",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fnt_main",
    "AntiAlias": 1,
    "TTFName": "${project_dir}\\fonts\\fnt_main\\primer print bold.ttf",
    "ascenderOffset": 0,
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Primer",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "459da94b-99db-4139-bb60-ab5ac62841b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 32,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "27a7548d-e5ac-4d7e-a2ee-6f0226841a11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 32,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 123,
                "y": 104
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "5e9b88ca-215f-4af7-993c-e0b764273d61",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 32,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 114,
                "y": 104
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "37c3362d-ab9e-40b7-95d4-55702b262565",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 32,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 97,
                "y": 104
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "ce4d12ad-d78f-4ca8-a862-7a36d4756105",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 32,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 84,
                "y": 104
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "c5bf887e-be69-4852-9a1e-85d474ce6c66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 32,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 68,
                "y": 104
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "8d73258a-b844-495c-a21e-4fe2882dc8f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 32,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 52,
                "y": 104
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "6f98c2ca-8d2b-4a4a-b463-895cd95dd959",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 32,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 47,
                "y": 104
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "52ea4370-540b-4e5c-969b-6e92bd8aec26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 32,
                "offset": 2,
                "shift": 9,
                "w": 7,
                "x": 38,
                "y": 104
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "92c17ca4-a78e-4b43-b810-86707a0601c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 32,
                "offset": 0,
                "shift": 9,
                "w": 6,
                "x": 30,
                "y": 104
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "3cd21d53-31c7-4f90-9767-d8b63057b0d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 32,
                "offset": 1,
                "shift": 13,
                "w": 10,
                "x": 130,
                "y": 104
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "52ed0af1-c6f0-451f-b2b9-f83f7838ab1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 32,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 16,
                "y": 104
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "c5372d88-fca6-46dc-9a20-1f3fa80d8613",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 32,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 238,
                "y": 70
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "d2381163-eb99-482e-bcaf-01be6093dd30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 32,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 227,
                "y": 70
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "5d9e4bc5-7ff6-4203-90ff-39a594fb671a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 32,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 221,
                "y": 70
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "2915ec2e-fe06-4e7c-82cd-44a8f82a5b8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 32,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 207,
                "y": 70
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "1483f5bc-168b-47f2-9514-cda7188c88db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 32,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 191,
                "y": 70
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "1ebd6542-f14d-4ee7-b9ec-eb3b5cea4eae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 32,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 185,
                "y": 70
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "4e218c6f-07ff-436b-adaa-601e8efd9ad0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 32,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 173,
                "y": 70
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "37d19e5e-743e-48b1-a219-a8494d7b98ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 32,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 160,
                "y": 70
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "03aece98-af71-4caa-8461-da0a5d8e9be6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 32,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 146,
                "y": 70
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "a2704321-d666-42a0-84d4-5086d3d238e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 32,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 2,
                "y": 104
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "ce1ae101-4969-4789-bb64-23815928e047",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 32,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 142,
                "y": 104
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "0639d71a-2a66-4ce0-8ab0-a563c7412a2d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 32,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 155,
                "y": 104
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "8d948fa6-b4ba-4bf8-a47f-0de461d6ebe5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 32,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 169,
                "y": 104
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "31083dae-0057-4259-93ac-2a3e720fbc0a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 32,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 218,
                "y": 138
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "b594388e-8bae-44bc-bd1e-9a93086908c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 32,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 211,
                "y": 138
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "5b73266a-ac2a-476e-9bcf-67b3a4c4e07a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 32,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 204,
                "y": 138
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "5d0f2304-8748-4cf3-a0a0-d913148ce61d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 32,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 190,
                "y": 138
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "d53f7a8e-ed6c-4a74-911d-cc25d51ed786",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 32,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 176,
                "y": 138
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "ecff72fb-21c7-4f83-aa96-2c1c3ee83130",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 32,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 162,
                "y": 138
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "64bb1728-1df4-4a87-bbe0-9b55e62b024b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 32,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 150,
                "y": 138
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "29c52ed3-20ae-4dcc-ae90-c54750fe8164",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 32,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 129,
                "y": 138
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "646f0333-0799-4a77-a4bc-8923b8b3ed78",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 32,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 112,
                "y": 138
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "e761f568-669c-49c7-84ae-b34d971d0745",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 32,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 96,
                "y": 138
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "07061980-bf71-4265-ada6-c6dc58250d08",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 32,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 79,
                "y": 138
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "fb174985-81df-4e7f-86c7-e1758aefcdc6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 32,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 62,
                "y": 138
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "2be89826-b540-473b-9459-dbb99c8f48dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 32,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 49,
                "y": 138
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "f151dd74-ca4a-4e00-adb4-ce96aa505692",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 32,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 36,
                "y": 138
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "1550d658-b77c-4e78-91d2-c5b1d1243b87",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 32,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 17,
                "y": 138
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "f160cd76-63e1-475e-8434-6c297261c57d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 32,
                "offset": 1,
                "shift": 16,
                "w": 13,
                "x": 2,
                "y": 138
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "fc4636b8-2892-443f-98d4-b38c9f928819",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 32,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 245,
                "y": 104
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "f5a88d85-576a-4024-a7af-1230de37549a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 32,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 230,
                "y": 104
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "c93d6952-bef6-4136-b1a7-e9e74e4712bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 32,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 215,
                "y": 104
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "9b88f407-23ab-426f-bbb4-3530cc1fffda",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 32,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 202,
                "y": 104
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "964595b9-a625-4288-83fa-ceed45513cae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 32,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 181,
                "y": 104
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "605525a4-a195-4111-9027-34c15e538e74",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 32,
                "offset": 1,
                "shift": 16,
                "w": 13,
                "x": 131,
                "y": 70
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "b304754a-ca0a-48b2-8795-b2d6a7bd6026",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 32,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 112,
                "y": 70
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "ca271c34-cc44-4509-ada5-1ebc5e1a6ffc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 32,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 96,
                "y": 70
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "b1dc7edd-559a-403e-9a7f-b5f957b6d02f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 32,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 56,
                "y": 36
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "cc1714ff-9ecb-4906-a9d7-22f20f411329",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 32,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 32,
                "y": 36
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "cb5402e0-b8ea-40d7-97d2-43b56d0d3818",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 32,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 19,
                "y": 36
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "93111cca-0811-43f7-a7f6-d559b619a69c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 32,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 2,
                "y": 36
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "bae263c2-947d-44f4-8bac-bb01cbf30ba3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 32,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 239,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "dd71a464-1b38-44fb-b162-ba5c9c2a2545",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 32,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 222,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "b0ebbb6b-4d31-4bad-940c-cf462244684f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 32,
                "offset": 0,
                "shift": 27,
                "w": 27,
                "x": 193,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "69cc7c8b-8ed2-4e9e-8f07-e803df92e3ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 32,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 176,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "b71eb41d-94c8-4d4e-8d0f-dc64ea1fbb12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 32,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 160,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "28aaaac9-30aa-4c21-a00f-06dd4de0c67e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 32,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 144,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "c0ccb88a-2dbe-4bfe-aa93-454f636b99c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 32,
                "offset": 3,
                "shift": 9,
                "w": 6,
                "x": 48,
                "y": 36
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "25398723-be7b-4123-a12e-70e7f2b0e215",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 32,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 130,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "a1ab08df-0774-4374-b094-c61f29156be1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 32,
                "offset": 0,
                "shift": 9,
                "w": 6,
                "x": 109,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "c6d12585-5943-4cca-8762-0ed96e4736b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 32,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 94,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "0bdc9037-6175-4849-991b-4afbc9e977da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 32,
                "offset": -1,
                "shift": 11,
                "w": 13,
                "x": 79,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "3b45866f-cb76-41f9-b5d1-6a2f1b811be5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 32,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 71,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "f3064106-df5b-4428-9867-44337c3c3ca5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 32,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 59,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "28449157-8708-47b9-9a02-47ec8e11bba7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 32,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 46,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "a6a901fd-a67e-413c-8471-795dbfdd81ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 32,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 35,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "501ddc23-c4eb-4e5c-87b6-e8a722de39c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 32,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 23,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "73982166-c9a6-489d-93fa-7183bfb9ac34",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 32,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 11,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "a799c50e-f670-49d8-96e3-91590840e1e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 32,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 117,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "064cd5d3-0e0b-448b-8767-ebcfc473a3ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 32,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 75,
                "y": 36
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "a10aa589-969e-4302-ac10-2ba93da52796",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 32,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 199,
                "y": 36
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "30d94b25-ce5a-4cf2-8c2f-eeb56555b0dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 32,
                "offset": 1,
                "shift": 7,
                "w": 4,
                "x": 87,
                "y": 36
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "04227a3e-bb94-440d-b002-77e1a7d6cc14",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 32,
                "offset": -3,
                "shift": 7,
                "w": 9,
                "x": 74,
                "y": 70
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "e8ebe960-cc23-4a6c-bd21-99048c735aa7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 32,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 63,
                "y": 70
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "9c495e6b-4fd0-439e-a31a-14174254c3d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 32,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 57,
                "y": 70
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "fe058f79-ae01-4503-b1f6-7d072dbe8a4a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 32,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 39,
                "y": 70
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "5009131e-5d23-49ec-8678-4dd3fe9682c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 32,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 27,
                "y": 70
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "41b721ab-3db2-4b87-b319-6cfe871c807d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 32,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 15,
                "y": 70
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "16487427-3a39-4326-8c6d-6c61fd15f075",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 32,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 2,
                "y": 70
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "a395b4f1-3f44-4f2c-866b-5ec10dbec2c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 32,
                "offset": 1,
                "shift": 13,
                "w": 15,
                "x": 233,
                "y": 36
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "fd79309c-94fc-40a2-8005-1ce38ad9706b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 32,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 222,
                "y": 36
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "9db88695-9edb-4774-98a9-f8bfc0574485",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 32,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 85,
                "y": 70
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "4509e1d5-2552-44ee-94c5-07eec7e5c5be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 32,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 211,
                "y": 36
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "3ecb1160-42ac-48f4-b929-d673bd7ee980",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 32,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 187,
                "y": 36
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "f922123d-298e-4794-8336-ef56598c13c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 32,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 174,
                "y": 36
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "a4c6ee4c-c075-4168-9256-b82272ef9124",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 32,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 153,
                "y": 36
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "c0a0a020-fb09-4891-849d-d19c22ae4d67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 32,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 140,
                "y": 36
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "254283ab-a7a2-4e23-a4cb-08451199361f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 32,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 127,
                "y": 36
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "109e4449-1f09-429b-988a-975cae9ca653",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 32,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 115,
                "y": 36
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "4d66d055-2316-4a19-919b-bdf1411c2e5b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 32,
                "offset": 2,
                "shift": 9,
                "w": 6,
                "x": 107,
                "y": 36
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "7eaba7d3-019e-46b3-8701-208545b8656f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 32,
                "offset": 4,
                "shift": 11,
                "w": 3,
                "x": 102,
                "y": 36
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "5efa877f-ea1d-491e-be5d-14b63f4760b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 32,
                "offset": 0,
                "shift": 9,
                "w": 7,
                "x": 93,
                "y": 36
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "c6dd19a5-9db8-4f02-848e-0d729370e914",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 32,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 231,
                "y": 138
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "eb8de235-8581-4823-9977-656989765919",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 32,
                "offset": 5,
                "shift": 26,
                "w": 16,
                "x": 2,
                "y": 172
            }
        }
    ],
    "image": null,
    "includeTTF": true,
    "italic": false,
    "kerningPairs": [
        {
            "id": "c60a34c5-b1f4-46fb-bb3d-b9e603cf998a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 74
        },
        {
            "id": "7b0048b7-c945-4736-a01c-3f1b2e96edf9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 57504
        },
        {
            "id": "bd5b89cf-f907-40d9-bddb-72e0e067ee2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 38,
            "second": 84
        },
        {
            "id": "6985c6d5-7b0f-4828-b347-6b28cffc4463",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 38,
            "second": 86
        },
        {
            "id": "2be1ea30-452d-4799-bc3d-cfdd7db54903",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 38,
            "second": 87
        },
        {
            "id": "c0ada0d9-d2a1-4659-8af0-02f843295a65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 38,
            "second": 89
        },
        {
            "id": "b75e35c4-f44b-4b02-86fc-cf65a1edc344",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 38,
            "second": 221
        },
        {
            "id": "aeb8b1b0-8495-4225-b834-1ef0aee34789",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 38,
            "second": 354
        },
        {
            "id": "89eb94d4-82d6-4c8b-ab41-71ad815df229",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 38,
            "second": 356
        },
        {
            "id": "8c7ddcc4-1724-456f-8e82-0fc92b3cc331",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 38,
            "second": 358
        },
        {
            "id": "70fe3fda-18cb-4d73-857f-f67b4c10de04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 38,
            "second": 374
        },
        {
            "id": "4d43defe-4816-40e5-ba72-df4a77b87dc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 38,
            "second": 376
        },
        {
            "id": "2e83bbd7-09f8-4557-b306-6252bf62c5ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 38,
            "second": 538
        },
        {
            "id": "8876895c-5fba-4aae-82bd-deb60005d4bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 38,
            "second": 7808
        },
        {
            "id": "9a8793cf-d32b-42f4-8b67-e40f139f5d11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 38,
            "second": 7810
        },
        {
            "id": "9e19d6d6-dbca-4646-aa55-bf8ef53bbb22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 38,
            "second": 7812
        },
        {
            "id": "0bfb4a8c-92f8-4b0d-af99-78cfdc020f72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 38,
            "second": 7922
        },
        {
            "id": "47e119e4-b601-4b5f-89c2-2c2e1e294dbf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 74
        },
        {
            "id": "301a48f8-d7a4-420b-943e-60500d710110",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 57504
        },
        {
            "id": "edd06477-6e31-44f0-8115-fc01af0ad7cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 57
        },
        {
            "id": "a4993f5e-94b2-460b-9987-4911c8b825b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 86
        },
        {
            "id": "7956f1f9-c95e-4fab-8312-0433127ad3f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 87
        },
        {
            "id": "0f6f1628-8acc-49fe-b50e-de4c13a99063",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 89
        },
        {
            "id": "e242fa70-a8ab-4e3e-8b10-0902f92bab1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 221
        },
        {
            "id": "b47af646-5ec4-4634-b42a-a2421dd53a7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 268
        },
        {
            "id": "1c35b85d-5d22-4ba3-8d95-9e36e6f2991c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 374
        },
        {
            "id": "a29b2be1-8aa9-4746-88c9-13e9a2153385",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 376
        },
        {
            "id": "9b81f678-8365-46d2-b641-eadf3169224d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 7808
        },
        {
            "id": "bda6f316-1556-47e3-9c95-0689764a101b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 7810
        },
        {
            "id": "5683600f-9777-49e5-ae87-4dfcc47742b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 7812
        },
        {
            "id": "dc1caf0d-b3a7-4334-97a9-f33b28d6eb22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 7922
        },
        {
            "id": "5fee0c1f-0f3e-42e4-b300-5dcd4c3a2e99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 50
        },
        {
            "id": "dd1b98c0-cc6e-45c2-b7fb-218ab7b2dd8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 74
        },
        {
            "id": "d4090f6e-0e08-4949-8d1d-bca496168cb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 83
        },
        {
            "id": "c85cdc80-31e7-40f9-9964-f878244deeb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 84
        },
        {
            "id": "24b0b9e0-8703-4cc6-b319-e7c719878b86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 88
        },
        {
            "id": "7a7fbdc6-388a-4627-8952-77c8420806d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 45,
            "second": 89
        },
        {
            "id": "ae7d9e21-1f47-4f63-aff0-07c0f6b8ada9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 45,
            "second": 221
        },
        {
            "id": "2b494ce4-ba9a-411f-9121-bc73ecc47742",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 346
        },
        {
            "id": "3895cde3-f778-42eb-97ef-e1260aa38b9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 350
        },
        {
            "id": "433135d0-c6d3-46d2-8f62-10bb830ce599",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 352
        },
        {
            "id": "4ec1989a-79eb-461e-a047-150f43233af8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 354
        },
        {
            "id": "bd98d73d-5502-4d18-88d9-92286272e98a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 356
        },
        {
            "id": "0ffd8a07-70f9-440a-81d1-52efd6e81a65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 358
        },
        {
            "id": "b707d470-cb58-4fcd-a111-716990c6012b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 45,
            "second": 374
        },
        {
            "id": "c1346fda-a3b2-41e8-a2b7-cd7eed65284e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 45,
            "second": 376
        },
        {
            "id": "59e5bee8-854d-4547-b529-eef95570dc1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 536
        },
        {
            "id": "cb969735-f21f-4813-8878-e2c30ecbf009",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 538
        },
        {
            "id": "fccaac0e-53b7-4628-9f69-20e2ea88deda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 45,
            "second": 7922
        },
        {
            "id": "b57b8580-b327-41c8-9ab2-69b56217958d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 57504
        },
        {
            "id": "ecc40498-0490-4824-ab1e-71897aed4aa8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 57
        },
        {
            "id": "509babba-6b8d-4a0d-b3fa-7a27edf2bede",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 86
        },
        {
            "id": "dbc742a4-cfc5-411b-b699-80a961494a8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 87
        },
        {
            "id": "aa067774-c018-4a02-bc02-7a57db6b8637",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 89
        },
        {
            "id": "8e98e77d-90db-4592-a9db-d289020966dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 221
        },
        {
            "id": "9cba741f-fbc1-4cba-b726-f4e0501100a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 374
        },
        {
            "id": "01afc487-aa19-4288-a511-301fd7fa4fc4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 376
        },
        {
            "id": "433e2134-ebc3-47dc-bf9d-b93a48e47493",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 7808
        },
        {
            "id": "9c9e8c6f-ebb4-474e-bc45-c10317fd410d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 7810
        },
        {
            "id": "29e45639-8bc2-4a15-8360-85c99ee140a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 7812
        },
        {
            "id": "7e403d10-49e0-4daf-a5c3-c5b21c54d7fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 7922
        },
        {
            "id": "24467244-1bf2-41fc-bf79-2afae371c3a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 47,
            "second": 65
        },
        {
            "id": "f0916490-4a13-48a1-8abb-a5b48e6fc37a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 47,
            "second": 74
        },
        {
            "id": "a713a373-cd29-4fc1-ad19-16c663ec9b0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 97
        },
        {
            "id": "471384e6-fe43-4396-9a10-9f3c0adab5bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 99
        },
        {
            "id": "51fc02a6-b323-4cbd-85a8-df45b5e696e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 100
        },
        {
            "id": "b900c8a4-6347-4a09-a50a-4083f7db8da7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 101
        },
        {
            "id": "d23eac79-57ca-4679-a2cb-f35facc309b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 103
        },
        {
            "id": "f22add46-4606-4940-bbef-f046a3551d67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 109
        },
        {
            "id": "8c3f1c6f-f593-46b0-8b07-48aad6c20111",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 110
        },
        {
            "id": "897676f4-917e-49d5-be40-f9680704e988",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 111
        },
        {
            "id": "65192ff7-c33b-4055-a2d7-5b78cfac2eba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 112
        },
        {
            "id": "054c5038-b770-4a2b-8a60-95826099a9ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 113
        },
        {
            "id": "d8c9d7c1-73b0-4cdb-8deb-93c61eccffb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 114
        },
        {
            "id": "9a65ef69-d310-4d9c-99a4-e103cf03eff8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 115
        },
        {
            "id": "4ba21c9d-4014-4f15-b746-c80e01b6d1a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 117
        },
        {
            "id": "d383ce83-84f9-4ca3-9c4a-94796bdb64ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 47,
            "second": 192
        },
        {
            "id": "13d11a3b-dbe4-4d25-9f62-3a1aac7d768a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 47,
            "second": 193
        },
        {
            "id": "6654c9ee-424f-460a-af41-011d31278132",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 47,
            "second": 194
        },
        {
            "id": "d6c85577-affe-4252-9a94-5a624716403a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 47,
            "second": 195
        },
        {
            "id": "46c6af3c-1ab1-48e1-995d-8b0584680e50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 47,
            "second": 196
        },
        {
            "id": "b223f139-c08c-4b21-9919-e45b4b810b34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 47,
            "second": 197
        },
        {
            "id": "e380a2e4-e02b-4f10-b8a9-aa76a94c2c0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 224
        },
        {
            "id": "b4066301-ec82-47b7-bbca-206d5773c16a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 225
        },
        {
            "id": "e7fb2066-bda6-4ea0-8bc3-5475137174af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 226
        },
        {
            "id": "8a946792-c3a0-46ba-aa85-362055807cfd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 227
        },
        {
            "id": "e7ab7172-4bfc-4ecb-b3ce-6ad88403e453",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 228
        },
        {
            "id": "318e12d8-b079-4dc7-8bdd-fc291a4e1ab6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 229
        },
        {
            "id": "5450427c-dbd7-4466-9a33-0de162f370f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 230
        },
        {
            "id": "9e9f898b-cf09-4dee-a0da-83a325c77ff0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 231
        },
        {
            "id": "10059684-2f33-47b7-8e79-7daf7e37ff31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 232
        },
        {
            "id": "d936ef41-386e-4319-91db-88227f4d45cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 233
        },
        {
            "id": "fa50b57c-5fce-470e-9492-53222a428cf2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 234
        },
        {
            "id": "c7a7de59-16c5-48ed-8440-7c934d61eda9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 235
        },
        {
            "id": "af0802cb-99b2-43d9-87e2-e164e4222e32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 240
        },
        {
            "id": "c786ae9a-191e-4b61-866b-0698e9044085",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 242
        },
        {
            "id": "42908ecc-2382-4beb-a4f4-100cc6f17332",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 243
        },
        {
            "id": "3ad532cc-8f7a-43c7-bc89-0137ec3d4f99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 244
        },
        {
            "id": "11b6529f-d150-4c4f-bf13-07f812021106",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 245
        },
        {
            "id": "7c614bf4-9361-4450-a960-cbece2024143",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 246
        },
        {
            "id": "8dc3fe79-2b7f-48df-961a-cc593b3b8d58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 249
        },
        {
            "id": "f2764d96-78de-40f8-ba22-73901006a9ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 250
        },
        {
            "id": "8bc26ccd-1ade-4122-a729-11bf3db75053",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 251
        },
        {
            "id": "0bbd33e4-c0e4-4366-bcfb-99c2aead77b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 47,
            "second": 256
        },
        {
            "id": "d474c14c-d1ec-4b74-b5ea-0a1997b5f62c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 257
        },
        {
            "id": "b17dec7a-faff-417c-9858-4606dd41058d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 47,
            "second": 258
        },
        {
            "id": "3b7ade8b-beca-4970-bd2c-98b6b82a8e2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 259
        },
        {
            "id": "d2c509d3-0621-48c3-ad67-bc6585bb84c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 47,
            "second": 260
        },
        {
            "id": "c225bb3d-63d1-4662-944e-fa8a27a76278",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 261
        },
        {
            "id": "ec0ff949-8bab-4bdb-b3ec-c47c100ff058",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 263
        },
        {
            "id": "bc5eb3b8-924c-4701-bb60-e1564e114b3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 267
        },
        {
            "id": "d7469e0a-4c48-4b58-a9e0-d504f53d6356",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 269
        },
        {
            "id": "4344dd40-f54f-44d0-b954-5f07e544404a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 271
        },
        {
            "id": "4ebad798-edaa-431e-9794-1bd97ca4415f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 273
        },
        {
            "id": "be48d0d6-26b5-468e-ab52-60f5b97524b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 275
        },
        {
            "id": "0f0578d9-f215-4989-b707-48d5dc5a8eac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 277
        },
        {
            "id": "91ebcc2b-1a7d-46d1-b15c-a1a0af925719",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 279
        },
        {
            "id": "3d337ce3-dbac-4be3-a11c-c752e77f63e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 281
        },
        {
            "id": "7d7908bb-1ad0-44eb-a103-41dea1dabc40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 287
        },
        {
            "id": "542fa63c-da51-4638-a586-417163c4336a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 289
        },
        {
            "id": "63b37d3e-0689-449d-ab99-859bc868d1d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 291
        },
        {
            "id": "e56141e8-65b0-43bf-b878-7bcd3ce138be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 324
        },
        {
            "id": "9a903ead-069b-4d69-9284-f58be40fd9aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 326
        },
        {
            "id": "a516da84-0a3f-4891-8e48-d78008e5dfbc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 328
        },
        {
            "id": "69abf746-ddb9-426a-9c6d-ecba534db895",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 331
        },
        {
            "id": "a3674f9e-c949-4e80-8a06-dce5724bc796",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 333
        },
        {
            "id": "dc337936-ef2a-4fd5-b727-136b2cdf3767",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 335
        },
        {
            "id": "87a9381f-196e-417c-88c4-32a196844927",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 337
        },
        {
            "id": "8613b5be-9202-4b1a-96d9-54311948cb82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 339
        },
        {
            "id": "6c581ab6-0286-46e6-b299-585c4637b88f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 341
        },
        {
            "id": "08016c99-7538-4440-8188-98f9227b68bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 343
        },
        {
            "id": "a94157b4-f749-4c8e-be5b-f023d3266b92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 345
        },
        {
            "id": "9dd4e91a-96a9-413c-b45f-c4ef585657aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 347
        },
        {
            "id": "b99076e2-4d42-4653-8bfc-a2aad0b1ac32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 351
        },
        {
            "id": "42392c13-e356-4c46-9821-6d50a65ef0f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 353
        },
        {
            "id": "62f8ba2f-be98-49b0-873d-fa6fef379b74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 361
        },
        {
            "id": "52f100e3-e266-4094-88b0-ded15cdeab19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 363
        },
        {
            "id": "f7709bac-0cf0-4c7b-b315-f2a8577ddc72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 365
        },
        {
            "id": "18c0e345-19f5-4a16-a570-765463fcf662",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 367
        },
        {
            "id": "11bd7fe4-2d8f-4671-bee8-8197b4431798",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 369
        },
        {
            "id": "616b22f1-f5b7-4a05-877f-32f8161c502e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 371
        },
        {
            "id": "cde60f21-b6fa-4e08-934c-a2dcd5ee24d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 537
        },
        {
            "id": "805bd56a-dc77-4604-a111-629c1445f3b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 47,
            "second": 57504
        },
        {
            "id": "b3e1d9ab-aeff-4dc7-8a9b-6f1974a56290",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 47,
            "second": 57505
        },
        {
            "id": "327f8625-8ac0-4ff8-bda2-5755ba691013",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 55,
            "second": 44
        },
        {
            "id": "7d3c7ca3-a61d-47d8-8075-b41e233e15e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 55,
            "second": 45
        },
        {
            "id": "863629bf-41e2-42cf-9c1a-d472f027e863",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 55,
            "second": 46
        },
        {
            "id": "9e62431c-b53f-46e5-a55f-cda8b460e965",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 55,
            "second": 54
        },
        {
            "id": "c3d9f048-4d9b-4438-8bc3-03fe934a5f59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 55,
            "second": 8211
        },
        {
            "id": "a0b1c6cf-7d2b-4423-a77e-5aa92f0db859",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 55,
            "second": 8212
        },
        {
            "id": "bd585c27-3cd0-44dd-8a21-50fc02ba67f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 55,
            "second": 8218
        },
        {
            "id": "157914c1-75e2-4d8e-bf0c-e7ddf70da72f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 55,
            "second": 8222
        },
        {
            "id": "7779942f-2433-42df-b612-b668a63ae866",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 55,
            "second": 8230
        },
        {
            "id": "aff030b0-e668-4459-a110-8e647d5c2b60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 63
        },
        {
            "id": "cf3bef3e-54fc-4a1a-9315-a28f6ba3a778",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 84
        },
        {
            "id": "ea04996b-e3ed-4ea6-8452-92f047128baf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 86
        },
        {
            "id": "8a93b4c7-4b71-453a-899a-7e49ac101912",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 87
        },
        {
            "id": "daf457e1-1d79-4f72-9184-54756c645bc3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 89
        },
        {
            "id": "aaeec305-4834-402b-bdfb-3a110462fbcf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 221
        },
        {
            "id": "ced45df2-40d1-4e88-b6dc-ac63da90e2d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 354
        },
        {
            "id": "c3f7da1a-9a87-4655-929d-c7259cccfedc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 356
        },
        {
            "id": "51a189ce-b2c4-4aa1-90ed-a8c2a459e8ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 358
        },
        {
            "id": "77713786-86e0-4add-9f06-8609b8c3f53c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 374
        },
        {
            "id": "2daccb77-06c7-4ead-aa53-dd521ab9b38e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 376
        },
        {
            "id": "042095f2-d6d6-46d2-8e8f-c2caeec57e8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 538
        },
        {
            "id": "6af64ff4-0cc6-4aea-9dbc-737eece9f8dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 7808
        },
        {
            "id": "15d9cbe7-904f-4246-bbbf-3074760287dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 7810
        },
        {
            "id": "fc319730-5cd2-4fb2-9bed-995588e10885",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 7812
        },
        {
            "id": "26d1e992-4ef2-4d05-a24d-836937c7ab93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 7922
        },
        {
            "id": "36d3f54a-f475-44a1-b0e2-a958383ceb59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 67,
            "second": 45
        },
        {
            "id": "bf3a34aa-478c-4f01-94c9-c10de6018030",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 67,
            "second": 8211
        },
        {
            "id": "1709fa20-8d6f-4b96-bff2-8c8fa8083414",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 67,
            "second": 8212
        },
        {
            "id": "6acba8a2-216b-45ba-848c-f58f7944ff80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 44
        },
        {
            "id": "6d95a792-ed5c-4754-98bf-78746af9fd8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 46
        },
        {
            "id": "288ea230-7fcd-4784-80a0-5c36e72b68ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 89
        },
        {
            "id": "7246d05d-fb9f-46ff-ae26-8c101eea726d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 221
        },
        {
            "id": "d5fbccdd-0386-4c17-9cd4-dd024f63e523",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 374
        },
        {
            "id": "c3a706e5-47d6-4e57-8386-4de5accf8b52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 376
        },
        {
            "id": "2cdc966e-d12c-418a-922a-48240ebde163",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 7922
        },
        {
            "id": "bd5a78da-98fa-44ba-b4cd-2c94a94e12c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 8218
        },
        {
            "id": "ecf9963f-6d7b-4718-88ef-b137162de91b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 8222
        },
        {
            "id": "404d296b-d8fa-4e20-8746-b7918eb671f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 8230
        },
        {
            "id": "b648cd1c-71ac-4011-bb58-9ea2038edd24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 38
        },
        {
            "id": "70c5a44e-46ec-49b8-bd01-56eaf9e675d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 44
        },
        {
            "id": "4720cf46-6b99-42d2-8321-8600c9c260d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 46
        },
        {
            "id": "93d32b61-deee-4896-aa20-648441e96404",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 47
        },
        {
            "id": "00a08f3b-71e8-4cbf-aafd-641a03451ec7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 65
        },
        {
            "id": "a4887352-0211-4676-9a08-5cc53e3ecdb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 74
        },
        {
            "id": "cf66794c-5940-4c3a-86db-7ada51a69b3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 97
        },
        {
            "id": "ac13c004-2c29-4a95-9071-b64a2d0a1130",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 99
        },
        {
            "id": "ae713de4-7a09-40e4-8fee-9b758df1c358",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 100
        },
        {
            "id": "e41f7b7c-c92c-4fbc-a69c-4da11f21b5ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 101
        },
        {
            "id": "aceb7ad0-fa5f-4af0-b5c3-70dbc695c27d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 103
        },
        {
            "id": "a8913d9c-0990-44fb-985d-2db5c9325c5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 106
        },
        {
            "id": "0652b30f-76bb-4250-b121-b054d72a128e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 111
        },
        {
            "id": "c15f8beb-5b3a-457c-881c-13408b70cde0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 113
        },
        {
            "id": "1217988e-b98a-477f-8609-28afba23f65e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 192
        },
        {
            "id": "1ca7e29f-c092-41ae-893b-4090a49a41b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 193
        },
        {
            "id": "0f1e7c17-48d3-4d47-9293-9c4446fb23b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 194
        },
        {
            "id": "e7cd3bf2-b62a-4c5e-aa61-02499f00401f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 195
        },
        {
            "id": "feb385ae-7cbc-4072-8f87-679102cbadbe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 196
        },
        {
            "id": "fa5509f3-4d5b-4764-ad1e-a2ebf617ed17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 197
        },
        {
            "id": "086ceeb8-c21f-4bc0-9fc8-55a937637a74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 224
        },
        {
            "id": "53c10688-3acd-469f-b8a9-fd9cbfbafbb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 225
        },
        {
            "id": "5feb2b0b-1478-4514-80c6-1c412ab40913",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 226
        },
        {
            "id": "553fdf93-2ac5-4b8f-a3a4-7ea63c9b599a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 227
        },
        {
            "id": "ed53c60f-53bb-4bbd-b6af-a3c83e8a235c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 228
        },
        {
            "id": "724942a3-4b98-4a36-83e0-3b1480f7ffe2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 229
        },
        {
            "id": "37d26c22-2fd9-4ca1-bbf9-1b937970cd8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 230
        },
        {
            "id": "7f3c1276-49d7-40be-bb6f-2fff48987197",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 231
        },
        {
            "id": "7a5baa0b-63b8-4b2f-a2a9-144f4ae0c0f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 232
        },
        {
            "id": "1a2fe2cf-2d3a-4054-86f3-b7dcee70c358",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 233
        },
        {
            "id": "f762be15-6192-48fb-b523-c1d037437b1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 234
        },
        {
            "id": "7ec1d160-203e-4fc4-8845-20ae9ad458c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 235
        },
        {
            "id": "7b0fe5d6-866d-4050-a379-bb6989a69661",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 240
        },
        {
            "id": "b2b5e884-bddf-4d26-8702-ff38def5376a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 242
        },
        {
            "id": "e0c2ec29-850d-4e6b-bdfb-0ab1d44effb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 243
        },
        {
            "id": "a1901e9c-6c95-4254-9a61-bbc900e3abce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 244
        },
        {
            "id": "c100a2f5-ab97-480e-8223-e14c8dd8e173",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 245
        },
        {
            "id": "714fb912-6f34-4809-ac04-0504dddc7ec9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 246
        },
        {
            "id": "5b4eed5f-39f1-4ee2-808c-23f6e32b2984",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 256
        },
        {
            "id": "74dc45ee-ba29-4759-b912-4095ebc6f2ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 257
        },
        {
            "id": "7227b7df-980e-48a8-a302-cc0a5c329d2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 258
        },
        {
            "id": "d6a57696-383e-4eb4-9b1b-1fff72c179fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 259
        },
        {
            "id": "ebfb7dfb-9247-47a1-b13f-2cf21b423970",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 260
        },
        {
            "id": "d4d56a95-12e7-4fed-bb97-51d16221bcff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 261
        },
        {
            "id": "b97054c4-363f-4bbe-97a9-61e0e63d0b94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 263
        },
        {
            "id": "8574c56e-74b9-4270-b479-d90adb3aa712",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 267
        },
        {
            "id": "f1058cf6-d460-4b57-afd1-9f611b41376f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 269
        },
        {
            "id": "ac598e34-f774-42e9-b570-b408081a727d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 271
        },
        {
            "id": "46c71f72-b289-4014-81b5-190bdcc970c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 273
        },
        {
            "id": "13a031f9-121f-4a98-b28d-e12a1235ff1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 275
        },
        {
            "id": "8f2e3a6d-2c0a-429f-93ff-ffe3d302439e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 277
        },
        {
            "id": "f6f7a3bd-dc28-4587-b6b7-d977dbf7a598",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 279
        },
        {
            "id": "c07d17ad-a205-42e4-ac86-e35eba6715fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 281
        },
        {
            "id": "1b280310-0433-457c-9cfc-7c002d4b3518",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 287
        },
        {
            "id": "dafbaebe-16ca-4cfc-9e31-1d4de3ba3cba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 289
        },
        {
            "id": "983a3bef-f4b4-42a6-a7ba-df08d95c918c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 291
        },
        {
            "id": "70f64cab-d805-461e-b025-47d0b7875675",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 333
        },
        {
            "id": "ce4516d7-31a5-40b3-ad46-14393d50be65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 335
        },
        {
            "id": "bb0e8ae9-06b4-4028-b388-f53527c695f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 337
        },
        {
            "id": "35906a60-37a7-4be3-be5e-1130f6bb8b75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 339
        },
        {
            "id": "a55cad38-b18a-4fa0-81ff-8ae70de42a4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 8218
        },
        {
            "id": "4e4f51ac-c778-4af5-bd97-786aa3072d99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 8222
        },
        {
            "id": "6548f6d0-c876-4503-b01a-4154a89f04e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 8230
        },
        {
            "id": "0b4a31c6-d2e4-4cbd-bca0-89c1df6cd54e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 57504
        },
        {
            "id": "55e09af4-01e0-416e-9021-33cdb260b1a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 57505
        },
        {
            "id": "a08a626c-c92c-40b3-b718-370876df22df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 89
        },
        {
            "id": "1fda0aad-4edf-4b54-ba44-8901c40e793c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 221
        },
        {
            "id": "52f13b3a-0c05-4fc4-aa4d-09e245bb0ba6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 374
        },
        {
            "id": "a8357ca8-a381-47bd-a418-3b7490516577",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 376
        },
        {
            "id": "7dedd821-41ad-421f-80e5-d202d2b4716e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 7922
        },
        {
            "id": "ba32a2e8-f0fe-4750-aea2-b0692950f268",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 75,
            "second": 45
        },
        {
            "id": "49d5fdbf-b55d-40d1-9188-742d98b1eff6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 67
        },
        {
            "id": "a88b8b22-5508-47db-a3d6-c6918d17f625",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 71
        },
        {
            "id": "b2367923-a7b2-4bcd-8e4b-5753ad04e7a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 79
        },
        {
            "id": "52c602f5-8c73-401f-9f97-41e22f513289",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 81
        },
        {
            "id": "c608478d-6a95-4c9b-b3be-1908d6837b65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 199
        },
        {
            "id": "9e50cb38-b5ee-4d61-b957-90e64e6646c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 210
        },
        {
            "id": "08e6dc9a-4f22-4e72-b898-8bb051f3ec46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 211
        },
        {
            "id": "347c0e17-efaf-4072-ae7d-7aaf4f1b48a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 212
        },
        {
            "id": "cfea82fb-18fc-49bf-86de-df02ffc3ddb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 213
        },
        {
            "id": "325ddecc-020a-4953-9446-865ae6e7324a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 214
        },
        {
            "id": "02cad10f-705a-4606-8d3f-2de4e709469a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 216
        },
        {
            "id": "39f2aa77-e0b9-4fce-b26a-0c3313b7eac4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 262
        },
        {
            "id": "05c0a8e3-4043-4fd8-95d1-b025efbc22a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 266
        },
        {
            "id": "adb17572-8d2b-457d-a46a-151aec18e30f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 268
        },
        {
            "id": "d924790c-c6b5-4f01-9aa9-4d5d0533a63f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 286
        },
        {
            "id": "e0ef9f20-2d43-47bd-8a56-72fab5efc2f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 288
        },
        {
            "id": "f4248555-7997-48d9-90d7-acc9976a11f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 290
        },
        {
            "id": "878055d9-2dff-4ffa-a05c-25f4be568d64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 332
        },
        {
            "id": "6d15099a-0826-4b4d-af8a-79b6096db69a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 334
        },
        {
            "id": "2ca13e4e-3b73-4605-a2f1-454c0f9f9f61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 336
        },
        {
            "id": "7e9e6e2a-0cde-49c7-94c7-6cb75ccceadb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 338
        },
        {
            "id": "78791daa-5933-4ad9-b144-5c308c2a6975",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 75,
            "second": 8211
        },
        {
            "id": "e7bf94bf-7423-415f-9ca8-db06331ba0e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 75,
            "second": 8212
        },
        {
            "id": "68d1fd1a-951c-4153-ae96-e0baddabcf4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 45
        },
        {
            "id": "0261b27c-eed2-4aae-9ac9-f59ff0592c61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 63
        },
        {
            "id": "a57d5a53-8e24-41cc-abe1-3d1d373cc324",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 84
        },
        {
            "id": "def130bd-1146-4c1e-858c-79e32be6b4d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 86
        },
        {
            "id": "7a7f745f-3a4d-4ef2-a4d8-a4911e21f353",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 87
        },
        {
            "id": "15fc0186-2738-4481-bf2c-df4393234902",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 89
        },
        {
            "id": "a0d3e893-183a-4333-bd7b-5966393d79e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 221
        },
        {
            "id": "9dfb9553-f623-4bbc-9489-f7eb6e2ca7d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 354
        },
        {
            "id": "ca997d35-5ebb-44c3-a8f3-fc8cff6aa386",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 356
        },
        {
            "id": "6a43798a-ffed-4dc3-bf58-b0e196a002bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 358
        },
        {
            "id": "03c1bbdd-c433-417a-8c65-e4b022f7ac39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 374
        },
        {
            "id": "0282a0b9-6b4f-41e5-a98c-a29c83c09dbb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 376
        },
        {
            "id": "da302723-96f5-4eac-8ec3-8ac004de1c35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 538
        },
        {
            "id": "9d55620e-67fc-438c-9aec-5028cf79b014",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 7808
        },
        {
            "id": "2c4ba178-04f0-48cf-81f6-85d8b636684e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 7810
        },
        {
            "id": "5737e9b6-73fc-410b-b727-486ee9560794",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 7812
        },
        {
            "id": "6f122ee6-bc04-4ed8-b258-1fff257020f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 7922
        },
        {
            "id": "20792463-57ad-48b6-b613-1f5ab294f56f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 8211
        },
        {
            "id": "4f7fc491-40e5-49be-9e45-232afbfdc2ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 8212
        },
        {
            "id": "13aa001a-407e-4e00-88af-331177698652",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 79,
            "second": 89
        },
        {
            "id": "d2c608ea-8900-46d7-af15-16f025914035",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 79,
            "second": 221
        },
        {
            "id": "bbca422a-3b74-45d8-b11f-dda845175ff3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 79,
            "second": 374
        },
        {
            "id": "027f925c-078c-4165-b813-3cfc973fc3e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 79,
            "second": 376
        },
        {
            "id": "c4580346-0920-4ebf-97f5-acb7c36ce595",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 79,
            "second": 7922
        },
        {
            "id": "7c827c31-bf50-4b52-84f5-124b8efd599f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 38
        },
        {
            "id": "7aa2ceed-1482-4dd6-ac1e-94fbfd69aca5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 44
        },
        {
            "id": "4c60f9ae-8606-4514-9059-fe6a67b7a345",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 46
        },
        {
            "id": "8176f8db-16e4-444c-908d-ff036db189d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 47
        },
        {
            "id": "b71fd6ad-c338-4745-92f9-44091407d4b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 65
        },
        {
            "id": "3f5b3f59-c71e-4c84-80c4-13149e486adf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 80,
            "second": 74
        },
        {
            "id": "d4cfe01a-db9d-4de9-b4e8-13d2e82ce307",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 97
        },
        {
            "id": "4f37371d-339b-4cae-bc76-3f7841be4a39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 99
        },
        {
            "id": "1cf252df-9aa3-4ac0-9501-8551fd51c340",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 100
        },
        {
            "id": "51a559b5-2b89-46b8-8fe2-55fa64079a96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 101
        },
        {
            "id": "614ebf7b-e3b5-48e7-a4be-fbf50db292e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 103
        },
        {
            "id": "3b08ee16-ce37-4f7b-80dc-7d4f8eb81c5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 111
        },
        {
            "id": "244218f6-b0fb-4845-a173-611676fee139",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 113
        },
        {
            "id": "f8241a85-742e-4dfd-beaa-a6e05e930a23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 192
        },
        {
            "id": "5f895933-ff86-4a2b-8a6e-c07ae6fa2113",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 193
        },
        {
            "id": "80c31a7f-66c5-48fc-acc2-f034faa7374d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 194
        },
        {
            "id": "bb15c4af-66fc-4747-9512-82a39ad76787",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 195
        },
        {
            "id": "e717608e-64c4-49a5-90b5-b9423af5a2b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 196
        },
        {
            "id": "669ec375-7cb5-4eca-90b4-553692473849",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 197
        },
        {
            "id": "8ca83a1e-849b-4144-9638-8758d6a2469c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 224
        },
        {
            "id": "91b57d69-d6d3-4e1d-884a-22159bd1fd58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 225
        },
        {
            "id": "679de8fc-47cd-464f-97b8-99cc096bfbdd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 226
        },
        {
            "id": "d9bf22dd-28f5-47cd-a31c-cf0facffc52e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 227
        },
        {
            "id": "007828f3-ef0a-47d9-95bb-d43d455013c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 228
        },
        {
            "id": "c8c4a1ee-5da6-4821-b56a-c68f2f3069fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 229
        },
        {
            "id": "96d59159-d91c-44a2-bc98-1d489f565820",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 230
        },
        {
            "id": "c103f166-0a15-487d-811f-14cf3141f333",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 231
        },
        {
            "id": "54c18eaf-df4f-4a1e-9154-8580593c036c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 232
        },
        {
            "id": "28a14538-b7c2-44c9-8b01-587e31f16731",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 233
        },
        {
            "id": "010d7983-1b4c-4a22-8b89-20d25b967310",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 234
        },
        {
            "id": "eaad838d-201c-4b02-b1a4-5ad3f2d4fab3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 235
        },
        {
            "id": "b0f1c4d4-cc07-45fd-b14a-bb4de1463a49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 240
        },
        {
            "id": "c197fa78-e3f0-42fc-b7fc-638fa5eb443b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 242
        },
        {
            "id": "6fa0c2ce-a731-4111-8c8a-39978d163e8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 243
        },
        {
            "id": "4a98a1ef-a2d0-4ec1-b824-abf7d3609552",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 244
        },
        {
            "id": "295a5d04-4930-4bc6-97fa-4d925c1e2ac5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 245
        },
        {
            "id": "4d72bc14-093b-46fb-a147-980fe062feb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 246
        },
        {
            "id": "59d2e6d4-f8cc-429f-8b06-a9b57bc8533e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 256
        },
        {
            "id": "f8414b88-e59b-4e76-b60b-97aca3b369fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 257
        },
        {
            "id": "99ff7d02-3ad8-425c-8870-2f8a2cfade17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 258
        },
        {
            "id": "c78079b5-7e23-4b45-90cb-aa05df9082d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 259
        },
        {
            "id": "f19ad9b1-c459-48a4-8012-b8f653ef6787",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 260
        },
        {
            "id": "38a6c483-3c1c-4316-a374-dedd8c365846",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 261
        },
        {
            "id": "d930adc2-747c-45bc-b6a3-5393a8d603a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 263
        },
        {
            "id": "28487702-0a11-4893-8e8e-8e93d508625a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 267
        },
        {
            "id": "c0b846a1-6e6d-4abf-91ba-51155c90d08e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 269
        },
        {
            "id": "bf3e2636-bcb6-4b29-943f-35a275873000",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 271
        },
        {
            "id": "148b5233-a026-442b-aac7-3200c221d087",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 273
        },
        {
            "id": "50f54e07-ff58-4ba1-9694-02bea9527771",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 275
        },
        {
            "id": "b574972b-f003-42a4-8de3-73e8f0ef8b57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 277
        },
        {
            "id": "bd91297b-d72f-4e3c-a0c0-8bd06f7528fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 279
        },
        {
            "id": "e6f0f563-cd01-4c25-8539-405911b236da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 281
        },
        {
            "id": "9ee85cf0-8f3c-4793-95f1-c311379c9c19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 287
        },
        {
            "id": "ee1e61d1-cfd1-4bb2-a823-bc4dca02b87b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 289
        },
        {
            "id": "2b2ba061-11f7-432a-99fe-0c4bd7531339",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 291
        },
        {
            "id": "4c99d709-5918-4bca-b39f-413ae0822d9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 333
        },
        {
            "id": "c899a21e-4f0c-4ea0-a629-120e756211b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 335
        },
        {
            "id": "bc1fa630-ed49-434f-b002-8e61e2aa73d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 337
        },
        {
            "id": "6c3c986b-0618-47cc-8e4e-110e24e834e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 339
        },
        {
            "id": "f0ad31db-31b4-426c-802e-1710e2af051a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 8218
        },
        {
            "id": "be5d2b2d-f2c5-4d3c-9301-3ce2f69808ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 8222
        },
        {
            "id": "6ef0c8b4-f672-4ec9-b09c-ed89ef89ffa8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 8230
        },
        {
            "id": "c1313f46-7d04-4c97-9a36-c957cab554c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 80,
            "second": 57504
        },
        {
            "id": "ef25a666-e7f1-45ec-8d4a-9b8a775c03f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 57505
        },
        {
            "id": "ba787152-e0f7-46d5-ac44-ad7d5878aa6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 89
        },
        {
            "id": "f48b4eef-e261-481a-a686-8dcc4e76da25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 221
        },
        {
            "id": "f347ace0-dc58-424a-b4fd-ab1ab31b63a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 374
        },
        {
            "id": "a2234926-5a9a-4927-be70-ec9eea530c38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 376
        },
        {
            "id": "ceb543c0-68ca-4108-92c8-a0ce383dc166",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 7922
        },
        {
            "id": "29d95141-cc01-4f82-84c8-8cf36d130720",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 38
        },
        {
            "id": "f7deba5b-5b44-4e01-b464-3d2b7ba644fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 45
        },
        {
            "id": "8a9bf953-32a7-488d-86f9-2eb01c159b9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 47
        },
        {
            "id": "a069638f-01b7-476a-884a-d0f44a530509",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 65
        },
        {
            "id": "6c1a4d5d-ffae-43f4-8d10-b617089126c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 74
        },
        {
            "id": "3e753cfd-8a9b-4fa9-93de-5fe058bd50c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 97
        },
        {
            "id": "9a5bdce6-d5a6-4786-9bae-999b3186fb10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 99
        },
        {
            "id": "052d60c1-1cc4-48b7-8a8c-35e92eecd99a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 100
        },
        {
            "id": "2fead7b0-f81f-4090-b9a8-a4e863556d7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 101
        },
        {
            "id": "8178c842-a556-43c6-b229-3b9024404db6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 103
        },
        {
            "id": "57006c64-7b36-4361-9af8-7435a998b437",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 111
        },
        {
            "id": "d465f505-17ba-468d-8ed4-a1ca8af6e0c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 113
        },
        {
            "id": "6a79ca98-7fe4-47a7-a0da-a15b8ce2edb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 120
        },
        {
            "id": "2f4d57c6-c267-4ed5-9410-6ca0d84a732a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 192
        },
        {
            "id": "30ac9554-41c2-4149-9e2a-4eca1ffe841b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 193
        },
        {
            "id": "86c73c16-7d09-4ed3-85e2-172f5c173688",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 194
        },
        {
            "id": "d3a1ce54-5eaa-4f11-8706-38d38ad0bcbf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 195
        },
        {
            "id": "698d3e60-85bf-4d9c-8d64-83cf1739f92b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 196
        },
        {
            "id": "1a533f85-1e3d-4a66-8a4e-58cd9dc1b06a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 197
        },
        {
            "id": "212d0c78-4669-4001-b835-30f27df418ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 224
        },
        {
            "id": "06bc8251-b719-4746-a597-99847ee36b1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 225
        },
        {
            "id": "2020c63d-6221-49ce-b844-243f80ca2d94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 226
        },
        {
            "id": "e3dcf134-a428-4f66-ab99-a6060f7dfd9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 227
        },
        {
            "id": "81e4beae-e1bb-4cd7-a7a3-21b61db647ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 228
        },
        {
            "id": "81ed19b5-51c7-461f-94a6-b3ee3a31ca91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 229
        },
        {
            "id": "0a5e71da-f007-4a19-80f2-1a2b127885c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 230
        },
        {
            "id": "03e0df2b-f78d-4627-af41-7fcdb67fe3ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 231
        },
        {
            "id": "0094b331-07bf-4303-b2af-c0b61232ba2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 232
        },
        {
            "id": "e9649881-a06e-43c5-9600-376653947723",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 233
        },
        {
            "id": "a352c95e-03fd-4ac2-aeb3-a1c0a4da2aa7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 234
        },
        {
            "id": "129b8dcc-760c-49bc-b05d-4fdfcb37fbd3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 235
        },
        {
            "id": "d91547ae-d0a5-4595-92f7-7449f1889985",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 240
        },
        {
            "id": "289922ea-d588-443e-95f8-f9488fb47b1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 242
        },
        {
            "id": "61afbbc7-9905-47a9-b5fc-4dfc42438d38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 243
        },
        {
            "id": "a4d55032-8f94-4aa0-8252-1d5d29518778",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 244
        },
        {
            "id": "42821003-484e-4d0c-a6c4-d55bdd18d356",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 245
        },
        {
            "id": "b1f5c62b-5a3d-42c8-a6fd-84b059665cdc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 246
        },
        {
            "id": "e42c2517-493f-4ec6-bb38-1565486c6376",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 256
        },
        {
            "id": "9993ec3c-0e7e-49e0-9ee8-ad8e4b284d08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 257
        },
        {
            "id": "56ce054d-0ff6-408d-b206-5ace301d846c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 258
        },
        {
            "id": "890ec3a3-67a8-4c7e-aada-3683270fb2ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 259
        },
        {
            "id": "3911fe53-2ffd-4fa6-83bd-fc04cb950453",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 260
        },
        {
            "id": "f71725d1-ceeb-4379-ab12-8c8873267c2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 261
        },
        {
            "id": "5f5f5672-51bd-4d56-9437-73b79ce49d6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 263
        },
        {
            "id": "eecd580c-77e6-4108-b506-b0d31ca1b117",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 267
        },
        {
            "id": "f232d4e4-9c70-487b-bca6-505205df0ce6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 269
        },
        {
            "id": "7899ba18-8fa4-48a4-a55e-51332ffd0c6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 271
        },
        {
            "id": "bf9e2a1f-c072-4c29-a929-bb506303a2ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 273
        },
        {
            "id": "1dcdfa21-a0a4-4170-8601-3a6afa1cf26d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 275
        },
        {
            "id": "879214d9-576b-44d2-a724-e985cd53d077",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 277
        },
        {
            "id": "aa5c287b-cf93-481a-a71b-93bba4c58478",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 279
        },
        {
            "id": "1b9c3e7c-d654-4fca-a9c1-286e92ae583f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 281
        },
        {
            "id": "faaf1748-28f3-44f2-94ce-94a4bfdcf4f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 287
        },
        {
            "id": "cbeb68ab-8944-4ca3-b926-faf1ad255258",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 289
        },
        {
            "id": "425e571f-6cc8-41ab-b4f7-974c1dd828d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 291
        },
        {
            "id": "8aa3ffb5-5194-4fef-9d71-9a0b5cc25fac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 333
        },
        {
            "id": "89968208-ffc0-4a1f-b526-96c3ea3b8b25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 335
        },
        {
            "id": "24726d55-e2b4-43a7-bf8e-ec473f9ac542",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 337
        },
        {
            "id": "c86f301a-59cb-4072-bcde-c036f3151d67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 339
        },
        {
            "id": "65def4d3-ed1b-47dd-99b9-006650d9e15c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 8211
        },
        {
            "id": "99f0a335-0df2-4237-9628-9e2276e3d1e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 8212
        },
        {
            "id": "c88afac6-876b-4ff2-b344-d7f27bf78978",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 57504
        },
        {
            "id": "cc3aa51d-41a2-4d8f-9070-ad64889c1ecd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 57505
        },
        {
            "id": "1e3cec6a-b373-4cb5-ab4a-6fdbd949d03f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 38
        },
        {
            "id": "afac4c4a-89e0-48a5-9794-55c69063b48d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 44
        },
        {
            "id": "8dc84511-f285-49e4-9db2-e8ea2b926821",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 46
        },
        {
            "id": "076c15e1-b320-433a-a625-3e8eaada35e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 47
        },
        {
            "id": "e2d7d88d-2a56-42c5-8861-14f0d959ce03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 65
        },
        {
            "id": "58bda07f-4ea9-4787-97bd-50aef206f153",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 74
        },
        {
            "id": "74165691-6240-4651-89c6-19b4c9e19f7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 97
        },
        {
            "id": "df92acd5-69dc-4813-bc07-47c7eb86599c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 99
        },
        {
            "id": "348d560d-26bb-47ad-a3fc-3ded2b5f8e51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 100
        },
        {
            "id": "c0140936-24da-4057-804d-201ccc648049",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 101
        },
        {
            "id": "2d800aea-aaef-49e0-8e51-db33c3cdf4d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 103
        },
        {
            "id": "7fd1f45d-30b3-4f63-a714-347e78004015",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 111
        },
        {
            "id": "ceae2b9e-fc7b-4c32-9f35-af5e6bc6264c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 113
        },
        {
            "id": "a1386f27-f72d-4e4f-9713-f1fb7d17fc55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 192
        },
        {
            "id": "87372656-b8aa-46d9-8324-c1a9f885acd4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 193
        },
        {
            "id": "27074147-e463-4dfe-927d-fa6602bb01a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 194
        },
        {
            "id": "d55a1b84-dcab-4b1f-8811-6d8508307b56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 195
        },
        {
            "id": "be0779d3-48d3-4bec-8a2a-382fa3b6b2fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 196
        },
        {
            "id": "96cdca20-bf2d-47fd-bb49-e21d66b8b00e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 197
        },
        {
            "id": "5d743ec7-084a-4651-bdd6-41eb227f8d9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 224
        },
        {
            "id": "70b7779d-62c4-4397-b540-947526b57e82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 225
        },
        {
            "id": "992c95d1-99e8-4546-bab3-7c407071d2e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 226
        },
        {
            "id": "bb023a8e-e56b-42ac-9e67-a7dd00163e68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 227
        },
        {
            "id": "9a3e74fa-183b-416a-ba10-ef47ae76f2c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 228
        },
        {
            "id": "7f2d6a10-cefd-42b6-b4d1-760df1e66feb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 229
        },
        {
            "id": "e31544e2-0076-4737-b099-7f80294567dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 230
        },
        {
            "id": "be5d2b30-cf4a-4d41-8b5f-36b3238d477a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 231
        },
        {
            "id": "5384005f-c926-4eae-883d-8e952efa0bbe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 232
        },
        {
            "id": "7c23307a-1d99-4535-a9d8-07110d772ecf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 233
        },
        {
            "id": "480eaa32-5484-4d20-8811-e167980254cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 234
        },
        {
            "id": "7f03afe3-b53b-4822-b15e-4f6614f76e81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 235
        },
        {
            "id": "db52c76d-b82a-4fdf-936d-10b5cf55e3ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 240
        },
        {
            "id": "a15d7060-4887-40d2-ac2e-f4990c778631",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 242
        },
        {
            "id": "997a285d-dbe3-4247-8675-7d0102febd23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 243
        },
        {
            "id": "e13c7e20-0510-48fa-9f8a-401b6ad519d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 244
        },
        {
            "id": "d01be5e7-f587-4ffb-82a3-200a276be30c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 245
        },
        {
            "id": "1da13b3a-8d9d-4cde-81bc-64f0b470fb23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 246
        },
        {
            "id": "6822a764-0c0d-4890-8b06-6a19ba611069",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 256
        },
        {
            "id": "ef79222d-9976-4c62-b814-d405e03c0faf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 257
        },
        {
            "id": "ffa6ff6f-dda8-4d73-abfb-02e3658bcd1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 258
        },
        {
            "id": "186e02c9-eb8a-4904-bda8-d667697452b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 259
        },
        {
            "id": "71a94afb-72c6-46c0-aac3-ccd79211abf5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 260
        },
        {
            "id": "d9f6618a-0ade-4101-b984-e647b2f4e8fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 261
        },
        {
            "id": "b071dd0c-92b1-4bb1-9825-e83aa26cdc46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 263
        },
        {
            "id": "be28521c-7e40-40c6-8165-db580e1ca1ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 267
        },
        {
            "id": "cf955fef-fe0e-4d5a-b5be-9b3f9587bf9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 269
        },
        {
            "id": "dbe63f0c-a014-434a-a726-0843a8997f0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 271
        },
        {
            "id": "f5486b13-2a95-440e-a6c8-4df8ddb34fe7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 273
        },
        {
            "id": "2ebcd4ec-c91a-434f-a413-d703a9d799fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 275
        },
        {
            "id": "052f09a4-3e82-4b3d-9a6f-14badec76c53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 277
        },
        {
            "id": "0c248c8b-23d6-4f1e-8383-b6faeecbe4bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 279
        },
        {
            "id": "cde04062-8e76-4be7-a404-0c99e6aea310",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 281
        },
        {
            "id": "d3697d62-fe9e-4ea2-b4f8-a547e18f6225",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 287
        },
        {
            "id": "8a61cab6-56f0-49d2-ad5d-4e367cb3176b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 289
        },
        {
            "id": "e2cd0aa7-55af-4bcf-a699-fe7f24e623b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 291
        },
        {
            "id": "94fe9955-0d04-45bd-ae1a-a07a365c1ce7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 333
        },
        {
            "id": "f354579f-6e91-4866-b9ba-3973e5b6097a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 335
        },
        {
            "id": "de873ce0-334d-494f-bbf3-c976f4a823ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 337
        },
        {
            "id": "88a7fa60-11e8-4f06-9bfe-142b941fce2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 339
        },
        {
            "id": "01a45402-8c2e-4654-8362-373734ac971e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 8218
        },
        {
            "id": "3509257f-0e60-455b-94c9-b2de52abec2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 8222
        },
        {
            "id": "aba0cc93-086e-4b8c-b6f6-40f01d40bd10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 8230
        },
        {
            "id": "b3ea5231-95f9-4fae-a43f-3e40c22580bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 57504
        },
        {
            "id": "2851bb56-606e-4b45-8fed-c2fac06ed302",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 57505
        },
        {
            "id": "0a899159-3ba5-4e8c-add7-515d25372aa7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 38
        },
        {
            "id": "b58fb280-08fe-41e5-8c48-28d88a7cc093",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 44
        },
        {
            "id": "03123dc8-7aa5-4942-8162-f391e5cb8354",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 46
        },
        {
            "id": "1313a229-b72e-4f98-925c-6ec47bea0272",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 87,
            "second": 47
        },
        {
            "id": "be159a64-72a5-4a8c-b6f2-564f562c1f40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 65
        },
        {
            "id": "80464b2c-284f-49dd-a669-9d8635b2db2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 74
        },
        {
            "id": "64de4101-af9f-4df1-b5aa-46f133dac8fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 97
        },
        {
            "id": "f01a0de4-7237-41f1-a396-babe61ddaa7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 99
        },
        {
            "id": "cdcfd5e9-6eba-4052-8013-3c10a2588f5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 100
        },
        {
            "id": "72dea291-b4f8-4aa7-b94c-9395025b9fd4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 101
        },
        {
            "id": "414d2306-abae-421b-829a-ed83bbe3b708",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 103
        },
        {
            "id": "91e90cad-fd89-42ce-a392-10ea4d34b131",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 111
        },
        {
            "id": "c18060a6-2064-482f-9fcb-b104e064723e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 113
        },
        {
            "id": "6ad6c838-af9d-4be7-b209-e76426bfa052",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 192
        },
        {
            "id": "d4646452-e2f4-4a8b-86b0-5b23b7ff0f28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 193
        },
        {
            "id": "36eb96c8-c42c-4645-9dac-741a84cd8932",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 194
        },
        {
            "id": "41806ba7-2924-4fbc-8b8f-a2e724c2aec0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 195
        },
        {
            "id": "23009540-43d6-4067-a745-728eaef76dca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 196
        },
        {
            "id": "6ef3551f-2e1d-44d0-ae8b-1a2b8fa702b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 197
        },
        {
            "id": "79668446-d84c-42cc-8111-efa580642995",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 224
        },
        {
            "id": "6999deec-8dd9-48e5-b8f3-f48049a5a527",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 225
        },
        {
            "id": "76b87099-f49e-4999-b1de-f8b0dd578539",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 226
        },
        {
            "id": "0f98d116-0e0c-4b40-b4c3-764f181ff11b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 227
        },
        {
            "id": "04d2d14d-a421-48dd-bd50-15393386cae8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 228
        },
        {
            "id": "f457bce3-fd24-437c-be4e-e83bc3b3828c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 229
        },
        {
            "id": "504035e8-9779-4def-a792-0e1a00991d38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 230
        },
        {
            "id": "2385c14b-9ff5-473c-8799-02dfdebfa1f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 231
        },
        {
            "id": "001f655a-d052-462a-958b-b144adb56562",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 232
        },
        {
            "id": "44458ed3-8107-4090-b173-9ee9bcd695c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 233
        },
        {
            "id": "e16c4a2c-153d-466a-ad8e-fbf59b97ee1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 234
        },
        {
            "id": "56a06ae5-0d37-4b93-9be0-8602e92f30cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 235
        },
        {
            "id": "bc855692-d292-4d53-adf0-31d83785b4d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 240
        },
        {
            "id": "b94e8a4d-3058-44f8-9f0a-6e2b1cf59c44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 242
        },
        {
            "id": "cf23697c-26d4-4326-9af2-25c6029917fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 243
        },
        {
            "id": "7cc97001-536a-4010-9497-6dde7730dabb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 244
        },
        {
            "id": "a4803d61-d2b7-42c4-8b0d-2972d0fe9216",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 245
        },
        {
            "id": "cffa825e-2c3a-4a59-ba38-91c8a0b04297",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 246
        },
        {
            "id": "a558aa5e-f6a5-4570-8ab5-7931ee8d256f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 256
        },
        {
            "id": "f267b368-b700-401f-8883-e7d7bbb8adcf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 257
        },
        {
            "id": "612555ff-1e52-44b4-b633-7649c44a7c49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 258
        },
        {
            "id": "a4d5ef65-9535-4652-b305-468fdf6cd29a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 259
        },
        {
            "id": "312b1d55-fb8a-45af-af39-2dd1d5be2dbc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 260
        },
        {
            "id": "7a23024c-7137-46ab-bb4c-83dd094afe7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 261
        },
        {
            "id": "64fcfa6c-842d-4468-aa03-0227d07f8c0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 263
        },
        {
            "id": "a40956cb-0326-4b2d-973e-2226eacfc12d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 267
        },
        {
            "id": "e12dcc3f-4ba6-4ed0-814c-7a9e23cfb1ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 269
        },
        {
            "id": "75edee20-a627-4b39-a494-b314a7ca8fca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 271
        },
        {
            "id": "c52e15c6-2ab1-4071-966a-000ca238a830",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 273
        },
        {
            "id": "66fd7d69-7c2e-4f41-a796-893fcfa6d10c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 275
        },
        {
            "id": "af4c88f3-c5b9-4a9c-a761-6b88864f9d09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 277
        },
        {
            "id": "b5cd4bf5-00fe-4cfc-9d31-ea2ba851f05c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 279
        },
        {
            "id": "1b8f60bc-20cb-44af-8ab8-9b34b565f743",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 281
        },
        {
            "id": "d82f2785-8180-4545-9728-8b80b88458d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 287
        },
        {
            "id": "332546b2-421a-4857-94ec-bb60f75d0c34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 289
        },
        {
            "id": "c665bf64-f903-4c9c-b07c-656a4b9c1601",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 291
        },
        {
            "id": "3d61b43d-3efb-4e62-900d-eb0805386983",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 333
        },
        {
            "id": "f7af40d6-e3a7-4a0e-9964-900d2b7245cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 335
        },
        {
            "id": "37ec03bc-d8e0-493d-a139-8b96323917b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 337
        },
        {
            "id": "798d22fa-e842-4f0f-90c2-846886b14823",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 339
        },
        {
            "id": "62234744-87c5-4427-ba40-af5c84f9dea4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 8218
        },
        {
            "id": "7c42f324-e582-4a4c-a800-dae0f35f97e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 8222
        },
        {
            "id": "c7157c13-b8b8-44f2-a0bb-02fa3e62c012",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 8230
        },
        {
            "id": "f3bac82f-fd6e-4659-9022-351a5f58eec7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 57504
        },
        {
            "id": "82124044-4a8d-4840-ae7a-d4b510203d3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 57505
        },
        {
            "id": "1d60a148-48f2-4d2c-8012-fe18cc8c5551",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 45
        },
        {
            "id": "ce119b8e-b42c-4c2b-b084-a3bf0911ee6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 262
        },
        {
            "id": "6f7133a6-ed38-43c2-8a5d-88d2371df067",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 8211
        },
        {
            "id": "c26b1cc8-e690-4eca-8024-2124c094a0ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 8212
        },
        {
            "id": "b77d5d3c-61d0-45b0-be80-1e588094d174",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 38
        },
        {
            "id": "a73bdd8d-e790-4eee-9908-1501c890f3b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 44
        },
        {
            "id": "a2c3ea46-0800-45a8-afc5-9aa97c5f1592",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 45
        },
        {
            "id": "990ce200-071e-4fc1-9af4-b87adf0b032f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 46
        },
        {
            "id": "31448965-4b10-4986-a796-466dd2a19ce2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 47
        },
        {
            "id": "2cb9d81f-d464-4930-8004-0b471aaedb20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 65
        },
        {
            "id": "7947b75a-423f-49be-ab6f-c23e13e540c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 67
        },
        {
            "id": "ab3af328-1ed8-46c8-a4f3-68dad78ed621",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 71
        },
        {
            "id": "26bdaefb-3fa8-42ca-8661-255b80ca3c3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 79
        },
        {
            "id": "0abb00c7-43e8-4e74-a2cf-e9afc25bb41f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 81
        },
        {
            "id": "17814bd5-09fc-48a3-8934-3b55fa26376e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 97
        },
        {
            "id": "b75562b9-cc13-4321-a801-5b4fe532c7b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 99
        },
        {
            "id": "0c4525d0-03e3-424a-964d-76483cc59b5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 100
        },
        {
            "id": "13d41f0b-9817-4c6f-b7ed-e0ee99e564eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 101
        },
        {
            "id": "85482dc9-2052-4c74-98b4-c265e937b181",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 103
        },
        {
            "id": "6a6a9503-e8ab-4f5a-87d9-3127028d4e28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 109
        },
        {
            "id": "c0256998-4ef3-4469-af5c-eef2421edf9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 110
        },
        {
            "id": "ef71c4db-67dd-4d58-abc4-ffb67021e181",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 111
        },
        {
            "id": "d66f9bbb-b684-4aeb-ad4c-b873511d852f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 112
        },
        {
            "id": "5d66c422-ffc3-4990-b811-d762ab11cbbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 113
        },
        {
            "id": "17c02b81-a4e5-437c-93c6-83200839b193",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 114
        },
        {
            "id": "b5c3d3e0-2c8d-4b87-ba7a-2e4b4d544555",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 115
        },
        {
            "id": "5464f7ff-329e-4d77-a83e-f510be85857e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 117
        },
        {
            "id": "aeacea32-1848-4139-8d8a-9207ab616dd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 118
        },
        {
            "id": "0c06247f-0a89-48a3-8911-68217373ed2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 119
        },
        {
            "id": "cda40d62-9be7-4e1e-8c93-f5daa8fcf718",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 122
        },
        {
            "id": "ce930b14-227b-413d-9290-cdf3b5d0c54c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 192
        },
        {
            "id": "bbb9fced-3fd7-4e99-b9ca-eb8e4fad28bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 193
        },
        {
            "id": "71999533-dbe4-4745-bd48-22dff9c18418",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 194
        },
        {
            "id": "829e09cf-c88d-451d-bd63-74b725d95713",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 195
        },
        {
            "id": "e6c66c79-c4b6-48b0-b2bf-a5f8bc44beed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 196
        },
        {
            "id": "134bdb31-451f-4e97-9035-76333b8707e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 197
        },
        {
            "id": "f38cd1db-8fc3-4a26-b3ac-62c0124430dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 199
        },
        {
            "id": "ccbc011e-d826-43b6-8b56-ebe627cca9c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 210
        },
        {
            "id": "911ad995-1c86-48fb-9f75-5948cbf7bbd5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 211
        },
        {
            "id": "56a48847-3543-4a84-bf64-0722dfaa17b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 212
        },
        {
            "id": "28765f8b-5c25-44dd-815c-ff61784df44e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 213
        },
        {
            "id": "df6e8359-ecca-40ed-9093-c9d730348d34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 214
        },
        {
            "id": "67e2837f-7bb7-4539-b246-45a6284720a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 216
        },
        {
            "id": "dd83c8cb-18ac-4fcd-88a3-e1ef79849fe7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 224
        },
        {
            "id": "e3b80600-c3bc-41e0-a935-f14851062453",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 225
        },
        {
            "id": "2f326022-115d-4998-86d4-3be92eb52731",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 226
        },
        {
            "id": "5089ce6f-778e-4fa0-a1b7-68f8683d90c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 227
        },
        {
            "id": "85140b63-f969-4a69-bb95-54cb6837a07c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 228
        },
        {
            "id": "6e80b258-fada-48d2-af1b-ce2e65ebba04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 229
        },
        {
            "id": "55c31b6c-66df-448d-b038-281f3c9adef1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 230
        },
        {
            "id": "6a6788a7-86d4-4277-b8dc-74d1420ff5e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 231
        },
        {
            "id": "64bf1356-c1e3-43b6-adc5-ee0673c19004",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 232
        },
        {
            "id": "d878ef6b-261e-4071-9586-61321d8137e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 233
        },
        {
            "id": "85a0af9c-8f83-499e-a1e3-973294962c63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 234
        },
        {
            "id": "9be60613-1ae6-42b6-b517-1f70ba489ba4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 235
        },
        {
            "id": "d907938e-1f50-4247-a7b4-3b49c4493f9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 240
        },
        {
            "id": "0d7202ec-4d76-49cb-9c29-7131fb21f67b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 242
        },
        {
            "id": "271d649a-2e80-4399-9ca4-1d1c216c8bc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 243
        },
        {
            "id": "df2fabb3-9428-440c-953e-6914c5010f56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 244
        },
        {
            "id": "6abd7b02-8f10-49b2-8e71-aba008b83832",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 245
        },
        {
            "id": "397877bc-4944-4c14-9267-3f54580d3f21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 246
        },
        {
            "id": "7dfe798c-4989-486d-8200-36da14e94b79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 249
        },
        {
            "id": "a628cb01-a86d-40c3-b4e4-d0e13cd914f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 250
        },
        {
            "id": "6cebbb7d-f753-4288-99ab-87d1690b6b80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 251
        },
        {
            "id": "4443d2bb-adf6-4ce7-a3c8-e0c55805fc70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 256
        },
        {
            "id": "51250ad0-1fb9-407d-8dc6-40e2a59a08b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 257
        },
        {
            "id": "53cf6f96-8257-4f6a-99a9-4e9f795c5a60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 258
        },
        {
            "id": "5e40e580-02d6-4ab2-a608-0185766de5e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 259
        },
        {
            "id": "5d4c222f-038f-440e-b9cf-b5152c5e1da2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 260
        },
        {
            "id": "1636e111-ecac-4240-90d2-09af3de2f5cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 261
        },
        {
            "id": "d24545e8-afc9-4ad3-a759-3044dcfe8b3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 262
        },
        {
            "id": "fa04335c-73b2-49d3-a551-ef7dba514102",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 263
        },
        {
            "id": "3bb5be72-1206-4157-9775-1ebf427dbe0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 266
        },
        {
            "id": "f9b101ec-0cdc-42f0-b3e3-9ccfad8a2b5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 267
        },
        {
            "id": "0de98da7-1077-4816-aa15-0b3e47ee2834",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 268
        },
        {
            "id": "442618ef-100b-4b15-8900-2e8f45718c44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 269
        },
        {
            "id": "17862744-d319-415f-9408-c9550e3ba394",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 271
        },
        {
            "id": "17b62e5d-8809-48ab-8e26-b19d2d647da3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 273
        },
        {
            "id": "4f6facf1-c30e-4b00-88b5-832487013e2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 275
        },
        {
            "id": "a3c94e7e-2b5f-4544-bb6d-ef7eca66b6c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 277
        },
        {
            "id": "68a38a0a-7b63-4a53-bf98-cfc62964a68c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 279
        },
        {
            "id": "5039cc88-4114-4d32-b016-62d385e9d03c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 281
        },
        {
            "id": "fca90c5a-57f8-4ab4-a694-976f84b8b7f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 286
        },
        {
            "id": "9c124010-1740-4e6d-9654-d8d9a2424c75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 287
        },
        {
            "id": "c2f9178d-caa5-4da8-b7b2-b586afa613c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 288
        },
        {
            "id": "e3f9dae6-001e-41d5-90f1-b14822eb5e54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 289
        },
        {
            "id": "026625a5-8dc4-455a-8f43-3a02402ce57a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 290
        },
        {
            "id": "d9316160-eec4-4ca6-a261-60e992fe43d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 291
        },
        {
            "id": "f34aa6b7-ab4f-40d9-aa62-4b62343d5261",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 324
        },
        {
            "id": "ab10c530-50d2-4fe1-a6a4-94b890b1b176",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 326
        },
        {
            "id": "1f329e3e-79ea-471e-b927-a9d0fe99041c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 328
        },
        {
            "id": "20e3f9df-a151-4213-b1f5-ab77382b277b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 331
        },
        {
            "id": "7c1de427-6a7d-44c5-b7e9-f26b3e31453f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 332
        },
        {
            "id": "d6b1596d-3972-440f-9392-5ec41d2e9ab6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 333
        },
        {
            "id": "07a606d0-6d05-44f2-8a49-ccf95e7807ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 334
        },
        {
            "id": "ed0047ae-e3e7-4c63-acbc-f13cf12266a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 335
        },
        {
            "id": "8f454b88-3bae-465d-91f1-350054e42403",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 336
        },
        {
            "id": "2fd9e25f-8828-454c-a63d-c4efe9643422",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 337
        },
        {
            "id": "8ec5a834-f461-48da-90f8-42accb8cb957",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 338
        },
        {
            "id": "c69bedb3-8a77-4243-9bca-827134300ea8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 339
        },
        {
            "id": "8afe1f4b-4e50-4be6-87e5-406a7f79a0be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 341
        },
        {
            "id": "7a953f7a-ae34-4acb-be57-320644c7c37f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 343
        },
        {
            "id": "dc87d93c-645e-4806-befe-e2cc211f1566",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 345
        },
        {
            "id": "daf1ffbd-ddd5-411b-942c-0c2e388f8daa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 347
        },
        {
            "id": "6ff7bba6-627a-4f32-9742-f01200ed4a8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 351
        },
        {
            "id": "49856c08-fa83-4e1b-aa1b-ce6a3ab2bd11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 353
        },
        {
            "id": "bcc5f8d2-1215-48c0-a96f-14a16ea39c5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 361
        },
        {
            "id": "e2a0b31e-d43e-4c62-8767-11bd541d3ec4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 363
        },
        {
            "id": "5f6d4336-fec4-4495-84a5-a7ded3bab1e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 365
        },
        {
            "id": "e932a658-ef70-45c0-b73b-fccfdd0d42ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 367
        },
        {
            "id": "2f1f0b12-95b0-4e8e-ba7c-9468684cce9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 369
        },
        {
            "id": "3d5a82a9-60fc-4eb0-9cc4-d920b1a51f38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 371
        },
        {
            "id": "32b1f4ca-a88a-45de-8e8e-92c58347ec8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 373
        },
        {
            "id": "258d6202-3950-4587-8b7c-cb4129005eab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 378
        },
        {
            "id": "3e1a6e56-3c25-417c-89a4-7f8d15bf0a8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 380
        },
        {
            "id": "80148f25-88c7-4255-833d-ad0d67d8b65d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 382
        },
        {
            "id": "68da96a9-9ec0-4000-86f4-a9d1ae971fdf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 537
        },
        {
            "id": "a43bea5c-7ead-46e5-83b8-ae4a98abeeda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7809
        },
        {
            "id": "b7888923-8486-4856-bb9e-f05c7d3e8c5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7811
        },
        {
            "id": "c995b733-14b6-4207-a3cd-67c634ffa34c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 7813
        },
        {
            "id": "b3f6b2c7-789e-4657-ad6c-cf94c2620409",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 8211
        },
        {
            "id": "052a9f70-3e1c-4019-a35d-853b086d275d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 8212
        },
        {
            "id": "6b1005ad-6a2e-4346-9272-339f1366f44d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 8218
        },
        {
            "id": "7db003cb-3e99-4747-b54f-00b32df449e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 8222
        },
        {
            "id": "2ec01cb4-de4c-402f-a576-59feb20fbf56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 8230
        },
        {
            "id": "ced3a052-99d3-446f-885b-e682ffe5a95c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 57505
        },
        {
            "id": "374b10aa-cb1c-4a48-b6be-916dfd64a953",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 91,
            "second": 106
        },
        {
            "id": "336fa4e1-8c52-489f-93e2-1d4c4dbc88d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 98,
            "second": 84
        },
        {
            "id": "5c5019df-99d1-4723-9802-214bbf120fbc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 98,
            "second": 86
        },
        {
            "id": "3210d478-d0b7-483f-b804-68a0207acc39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 98,
            "second": 87
        },
        {
            "id": "dfad1d83-bd45-44ca-a493-18ca89a3956f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 98,
            "second": 89
        },
        {
            "id": "a20d675c-8817-472e-8af5-b35866ddfb2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 98,
            "second": 221
        },
        {
            "id": "603b280b-f1be-4b36-820c-ecb95784afc4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 98,
            "second": 354
        },
        {
            "id": "39669d0c-976d-459f-aefb-af272b4c8c71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 98,
            "second": 356
        },
        {
            "id": "828df53d-b0c7-494b-accb-2cd8046d9172",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 98,
            "second": 358
        },
        {
            "id": "c05674c7-3c42-4e77-8d31-96e053d01cab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 98,
            "second": 374
        },
        {
            "id": "e0286d92-c04f-4c15-9d1f-4b5e2cf224d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 98,
            "second": 376
        },
        {
            "id": "a0ef795a-d14d-436b-b29f-393a2d2fe141",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 98,
            "second": 538
        },
        {
            "id": "3b29b749-c804-411b-8f80-13e87b308f02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 98,
            "second": 7808
        },
        {
            "id": "3179c06e-5515-4d6f-8f72-dca760e0c04c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 98,
            "second": 7810
        },
        {
            "id": "d402e150-4879-437e-9521-e1c444ec73fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 98,
            "second": 7812
        },
        {
            "id": "d44c42ff-3a15-4030-89f8-9e686a35f500",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 98,
            "second": 7922
        },
        {
            "id": "25e0de55-7568-4c57-8a58-50b78a01732f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 101,
            "second": 84
        },
        {
            "id": "ebbb6f56-1b1c-473f-adcc-eaf9bfc0e935",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 101,
            "second": 86
        },
        {
            "id": "8e6d8d11-b8a0-49d2-9edf-9d3f67449132",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 101,
            "second": 87
        },
        {
            "id": "12c69a54-b848-4cde-aecf-ae9f0fee6eae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 101,
            "second": 89
        },
        {
            "id": "af106553-f4c2-4733-a539-03e09eeec2d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 101,
            "second": 221
        },
        {
            "id": "9300674b-1fb1-48f0-9991-d195fdace848",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 101,
            "second": 354
        },
        {
            "id": "51f181a4-7b65-44ab-99da-e2cf81a4fa62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 101,
            "second": 356
        },
        {
            "id": "f638e326-3aea-4c57-a270-7ed722097f12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 101,
            "second": 358
        },
        {
            "id": "ca424025-92f1-4f37-a600-52a8c3f0c831",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 101,
            "second": 374
        },
        {
            "id": "47d1733f-c11e-443e-9896-e1ccee4287dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 101,
            "second": 376
        },
        {
            "id": "24a9faea-dc29-48d9-943a-72993e18d109",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 101,
            "second": 538
        },
        {
            "id": "363f80e2-6907-4ac0-8f44-fa19f1869868",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 101,
            "second": 7808
        },
        {
            "id": "7dd087ea-1850-4cb3-aa3d-375fb02c59ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 101,
            "second": 7810
        },
        {
            "id": "0b8d1614-021e-4069-9421-6a741049b816",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 101,
            "second": 7812
        },
        {
            "id": "0e517910-b1c5-4ced-93af-0fa07c3589cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 101,
            "second": 7922
        },
        {
            "id": "de0f5627-78e2-409a-8ad0-59aab34e491a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 102,
            "second": 44
        },
        {
            "id": "caff9610-2416-4194-90b8-d6aec3183a98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 102,
            "second": 46
        },
        {
            "id": "02bf7fa5-9ffa-4185-b3ed-d50331771656",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 102,
            "second": 97
        },
        {
            "id": "f44d8512-d6ec-4163-8229-47b97dd88fa3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 102,
            "second": 99
        },
        {
            "id": "5306dce1-2658-4580-b34a-2f029d413926",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 102,
            "second": 100
        },
        {
            "id": "dee65beb-6643-46c0-9872-c515ebd459f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 102,
            "second": 101
        },
        {
            "id": "53137384-ee7e-4c9f-a87e-418b8b858f41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 102
        },
        {
            "id": "2011bbe2-78fc-406d-a0e3-91ed2caae323",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 102,
            "second": 103
        },
        {
            "id": "b0806181-41bf-4d11-8e2a-b94abd8faa95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 102,
            "second": 111
        },
        {
            "id": "587a8439-599a-46a5-94b0-3fe84fefc779",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 102,
            "second": 113
        },
        {
            "id": "c8c7bee5-0b12-49ba-959f-74fac1a8f70a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 115
        },
        {
            "id": "b312015f-daa3-4d13-a5e9-0749a019e6e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 116
        },
        {
            "id": "7fbd7a81-5a9c-491f-b6a3-128d3cf61a5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 102,
            "second": 224
        },
        {
            "id": "4b15c220-6ac2-4417-8446-f610294d10e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 102,
            "second": 225
        },
        {
            "id": "ccd7cbad-5ee5-49b2-838a-1a536e3fbfce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 102,
            "second": 226
        },
        {
            "id": "fee43e3b-df14-4a93-ab86-65daf3a9da55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 102,
            "second": 227
        },
        {
            "id": "582ce10e-dadf-401e-a8f8-8998f16af0f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 102,
            "second": 228
        },
        {
            "id": "dbfc77f3-4b7b-4d8f-bea5-a41735dea894",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 102,
            "second": 229
        },
        {
            "id": "a9285d34-95b5-407e-865f-9ab4cb08c7f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 102,
            "second": 230
        },
        {
            "id": "53c1990c-b2a4-4032-b18c-aea5fef16939",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 102,
            "second": 231
        },
        {
            "id": "234af2e5-5bfa-44aa-814d-f1cbe17b8456",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 102,
            "second": 232
        },
        {
            "id": "298307a8-f1a3-45d6-b545-0104b1ad182b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 102,
            "second": 233
        },
        {
            "id": "10130668-4663-4467-9a5d-633e0cb0465f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 102,
            "second": 234
        },
        {
            "id": "105f7e24-6ec3-4d17-93af-ecab90c5c8f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 102,
            "second": 235
        },
        {
            "id": "48a2f8ce-ee08-4c8b-87e3-7ee8286efb7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 102,
            "second": 240
        },
        {
            "id": "48ff48cc-4500-4d7c-8607-a93e96455325",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 102,
            "second": 242
        },
        {
            "id": "33ccb35b-cb45-4fe7-ae42-9dab526067ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 102,
            "second": 243
        },
        {
            "id": "24780f12-7cfd-47fc-b557-918649bfeedd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 102,
            "second": 244
        },
        {
            "id": "2fd9141d-96d4-4309-b5a5-a398d46dfeda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 102,
            "second": 245
        },
        {
            "id": "3a3359be-3d81-4708-9428-23d640a8a06c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 102,
            "second": 246
        },
        {
            "id": "122a1a7b-2782-4c0a-b09f-4d0174e0e0af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 102,
            "second": 257
        },
        {
            "id": "d7841629-a686-4772-83dc-4704578f40b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 102,
            "second": 259
        },
        {
            "id": "836044ec-cfd2-4062-9aee-46a4e41aecfe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 102,
            "second": 261
        },
        {
            "id": "90f9571b-3555-4d5a-baae-9f4d53f87111",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 102,
            "second": 263
        },
        {
            "id": "5dc9ad0d-56f6-406d-8f5a-3b1923883591",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 102,
            "second": 267
        },
        {
            "id": "3463f22f-0bb5-4186-9cf3-4ffb8350b063",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 102,
            "second": 269
        },
        {
            "id": "709c5239-3256-4306-8e46-bde393587d0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 102,
            "second": 271
        },
        {
            "id": "c2188bba-468a-40fa-a8e8-62b8e4183caa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 102,
            "second": 273
        },
        {
            "id": "45c15cc0-35f2-4f83-9e1f-5dde1a70f18c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 102,
            "second": 275
        },
        {
            "id": "6b618649-0e51-4a1a-854a-7c536d6e335d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 102,
            "second": 277
        },
        {
            "id": "6e0632cf-16d4-44fb-b926-9ba9b0ef8744",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 102,
            "second": 279
        },
        {
            "id": "27db6be4-69dc-4772-951b-846de5fc421e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 102,
            "second": 281
        },
        {
            "id": "68207a0c-958e-45d3-ac83-5c5abd559740",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 102,
            "second": 287
        },
        {
            "id": "089b84f7-f537-4569-b2bd-4dd03ba3d3d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 102,
            "second": 289
        },
        {
            "id": "79ca9e62-76cf-4c32-8a05-d0d609fbacb2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 102,
            "second": 291
        },
        {
            "id": "f3084a65-15d8-4ede-9687-265af34c6fb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 102,
            "second": 333
        },
        {
            "id": "5a3a6cdf-9e74-4b13-a455-ff79bc0dfcec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 102,
            "second": 335
        },
        {
            "id": "faf0b318-882e-45d3-9e46-439f6ff47e07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 102,
            "second": 337
        },
        {
            "id": "45b713d9-3af0-49ca-b2ae-3b5ed7535a04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 102,
            "second": 339
        },
        {
            "id": "4ce4fc4b-7f38-4948-ada6-a4b31264beff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 347
        },
        {
            "id": "9a96a41c-5306-4b9c-9591-95b895937fdb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 351
        },
        {
            "id": "5b7e606b-f83a-4498-b315-bdce815d9eb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 353
        },
        {
            "id": "5b9328f9-18ce-4b01-9c51-2e2d18911fb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 355
        },
        {
            "id": "d87a274b-ee6d-47d1-a9e7-fe06c4727273",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 537
        },
        {
            "id": "5ce5b16c-557d-42f0-9d98-d966bf283d72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 102,
            "second": 8218
        },
        {
            "id": "1749ba01-88c0-4516-864a-109669b3af90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 102,
            "second": 8222
        },
        {
            "id": "a0022c78-a92f-4391-87a3-fad5a0fcfaea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 102,
            "second": 8230
        },
        {
            "id": "47a6d0c8-2a16-4168-a415-c96b445d3d1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 102,
            "second": 57505
        },
        {
            "id": "b94d7bb0-1941-4a51-974f-24f758714a23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 111,
            "second": 84
        },
        {
            "id": "aef0fdc5-59d4-4414-8539-0cebbf6eaefd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 111,
            "second": 86
        },
        {
            "id": "ebc9b510-b823-4ac9-8747-dee26bf3c7d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 111,
            "second": 87
        },
        {
            "id": "e93b577a-ce13-4623-a41d-725c526daa96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 111,
            "second": 89
        },
        {
            "id": "aa97ab99-58d4-4250-a7da-34c8364a6008",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 111,
            "second": 221
        },
        {
            "id": "f6604ec7-ff21-4447-9e7c-4279211c09a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 111,
            "second": 354
        },
        {
            "id": "0b8cb44f-121a-403b-8e64-9884003e451f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 111,
            "second": 356
        },
        {
            "id": "c6e98fbc-d58d-44a8-b3b0-48c9506b7a4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 111,
            "second": 358
        },
        {
            "id": "0b546387-e93e-4db3-aea0-f1891e0ccc12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 111,
            "second": 374
        },
        {
            "id": "3df0e4d0-6fec-4678-9d43-964c0857d016",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 111,
            "second": 376
        },
        {
            "id": "3ef956e0-d8bf-4545-9abd-ee5e6f088e1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 111,
            "second": 538
        },
        {
            "id": "7f3049ce-1767-4bf8-89e6-7c5f38917174",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 111,
            "second": 7808
        },
        {
            "id": "b75a71db-0a40-4904-809c-f38e122db871",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 111,
            "second": 7810
        },
        {
            "id": "ae6256a5-8f9e-438b-8caa-3eb442f5b7fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 111,
            "second": 7812
        },
        {
            "id": "ad0d47e8-ab48-4140-b062-061628889bb6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 111,
            "second": 7922
        },
        {
            "id": "0e342bfb-d0a3-4e6d-8390-bfe1d2611328",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 112,
            "second": 84
        },
        {
            "id": "bf244bfe-bd4a-42fd-a7ea-98c865a3d4ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 112,
            "second": 86
        },
        {
            "id": "4dd7be58-d514-47c0-97bb-c48428bca990",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 112,
            "second": 87
        },
        {
            "id": "60ad141a-01db-4c77-b55f-6db94ee8cb88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 112,
            "second": 89
        },
        {
            "id": "a34db492-39a8-45ce-8757-805808a1a2e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 112,
            "second": 221
        },
        {
            "id": "5533e307-341f-4a03-a1de-44571e7c365b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 112,
            "second": 354
        },
        {
            "id": "b011f2da-7279-4908-a2ff-0c76c44474b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 112,
            "second": 356
        },
        {
            "id": "358950af-c898-48d4-adc0-b1ea94b92ee1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 112,
            "second": 358
        },
        {
            "id": "c5630da7-1c41-4837-8c21-fcc8573aadf7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 112,
            "second": 374
        },
        {
            "id": "01c7cc2e-71c3-4aa7-997b-b0a06b51bf5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 112,
            "second": 376
        },
        {
            "id": "aa33fecb-83d5-4146-b0be-89b9d6d9a82e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 112,
            "second": 538
        },
        {
            "id": "a55792bd-ebdc-4007-8ca0-535bb5ae9c13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 112,
            "second": 7808
        },
        {
            "id": "43fa3e52-a4fb-4d6b-bb29-e09e3b0ea8c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 112,
            "second": 7810
        },
        {
            "id": "0ac8fd78-20ca-4399-8a09-5ec6e192ee36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 112,
            "second": 7812
        },
        {
            "id": "992da8ed-a9e7-4cfe-bccb-83bc008bfbb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 112,
            "second": 7922
        },
        {
            "id": "9bce8426-83a5-4c34-88b0-d7721a532c28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 6,
            "first": 113,
            "second": 106
        },
        {
            "id": "01c308a3-8c43-45b9-8728-6e7c81c27e29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 113,
            "second": 121
        },
        {
            "id": "7ac7cbbd-fdc7-4b54-b855-0dc692afd880",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 113,
            "second": 253
        },
        {
            "id": "85dffb47-1ff4-416d-b624-a9f0972bc489",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 113,
            "second": 255
        },
        {
            "id": "8279bcba-0a57-45ed-b15d-19211bf139b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 113,
            "second": 375
        },
        {
            "id": "9f443ece-c812-4605-ad2a-62d83be5d199",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 113,
            "second": 7923
        },
        {
            "id": "c9e533f2-4f68-4a1d-97d2-34317fef513b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 44
        },
        {
            "id": "839a62bd-31df-46a4-801c-f3fb8bfe4b6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 46
        },
        {
            "id": "bc763c92-1b57-4d7c-8a34-254b5ecf5a6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 8218
        },
        {
            "id": "8f84641a-1897-48ce-bc30-b4ed0924657d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 8222
        },
        {
            "id": "87af824e-0622-42a7-9a74-4b3904611cf0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 8230
        },
        {
            "id": "aeee6ac7-23f0-4b93-8b4f-ca80987f3e6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 84
        },
        {
            "id": "5f2b011e-db1c-428f-a6d6-d2d7f2e8367d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 354
        },
        {
            "id": "866cb6a5-49e0-4117-8131-0c96f9daab3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 356
        },
        {
            "id": "a76ab999-d814-4ce2-8f50-bd9a99246682",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 358
        },
        {
            "id": "b2152316-984b-492d-aa2a-16a53403b4b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 538
        },
        {
            "id": "a50df034-1ff5-4129-af44-2c2be714aa48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 119,
            "second": 84
        },
        {
            "id": "15dd1c48-bbc0-4d10-a698-a69c2f5f8fe3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 119,
            "second": 354
        },
        {
            "id": "b086924c-0acf-4019-b2d9-ff30b9a5f598",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 119,
            "second": 356
        },
        {
            "id": "17bc6254-310f-4698-9f6a-6e22642f5534",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 119,
            "second": 358
        },
        {
            "id": "89693f28-5564-4806-be2c-010143c0b335",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 119,
            "second": 538
        }
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000a\\u000aDefault Character(9647) ▯",
    "size": 20,
    "styleName": "Print Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}