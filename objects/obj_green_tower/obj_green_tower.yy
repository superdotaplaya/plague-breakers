{
    "id": "cae81964-d4ac-454f-a5a2-c281915d634f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_green_tower",
    "eventList": [
        {
            "id": "7e51d06e-f727-490c-87ae-ed66810c3fac",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "cae81964-d4ac-454f-a5a2-c281915d634f"
        },
        {
            "id": "265d07f8-4c3b-444f-9632-58f8a3d8c549",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "cae81964-d4ac-454f-a5a2-c281915d634f"
        },
        {
            "id": "357189bb-ca41-412a-aeb4-7197d776b57c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "cae81964-d4ac-454f-a5a2-c281915d634f"
        },
        {
            "id": "3d87b79e-3f8d-4728-a91d-180d6ad8e995",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "cae81964-d4ac-454f-a5a2-c281915d634f"
        },
        {
            "id": "63c87ac8-906d-4051-ade1-45fb5e3ef8fe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "cae81964-d4ac-454f-a5a2-c281915d634f"
        },
        {
            "id": "09ede52a-594a-4c58-9e81-5e8495c3d10d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "cae81964-d4ac-454f-a5a2-c281915d634f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ff17cdf8-f43a-45c6-8068-c71366134d73",
    "visible": true
}