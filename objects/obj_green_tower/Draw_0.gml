/// @DnDAction : YoYo Games.Drawing.Draw_Self
/// @DnDVersion : 1
/// @DnDHash : 73BF8011
draw_self();

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 4B458BDE
/// @DnDArgument : "var" "hover"
/// @DnDArgument : "value" "true"
if(hover == true)
{
	/// @DnDAction : YoYo Games.Drawing.Set_Alpha
	/// @DnDVersion : 1
	/// @DnDHash : 1DE5C2AE
	/// @DnDParent : 4B458BDE
	/// @DnDArgument : "alpha" ".5"
	draw_set_alpha(.5);

	/// @DnDAction : YoYo Games.Drawing.Draw_Gradient_Ellipse
	/// @DnDVersion : 1
	/// @DnDHash : 277347EB
	/// @DnDParent : 4B458BDE
	/// @DnDArgument : "x1" "-110"
	/// @DnDArgument : "x1_relative" "1"
	/// @DnDArgument : "y1" "-110"
	/// @DnDArgument : "y1_relative" "1"
	/// @DnDArgument : "x2" "110"
	/// @DnDArgument : "x2_relative" "1"
	/// @DnDArgument : "y2" "110"
	/// @DnDArgument : "y2_relative" "1"
	/// @DnDArgument : "col1" "$FF0AFF0E"
	/// @DnDArgument : "col2" "$FF7FFFA3"
	/// @DnDArgument : "fill" "1"
	draw_ellipse_colour(x + -110, y + -110, x + 110, y + 110, $FF0AFF0E & $FFFFFF, $FF7FFFA3 & $FFFFFF, 0);
}