/// @DnDAction : YoYo Games.Instances.Destroy_Instance
/// @DnDVersion : 1
/// @DnDHash : 7BE94FC1
/// @DnDApplyTo : 93dda09b-b386-4ff7-9efe-c2fd7ddb9a5c
with(obj_green_tower_dmgup1) instance_destroy();

/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 5620798F
/// @DnDArgument : "code" "global.upgrade_current = "green"$(13_10)global.current_tower = layer_instance_get_instance(self)$(13_10)instance_create_layer(95,830,"Instances",obj_upgrades_list)$(13_10)global.x = x$(13_10)global.y = y$(13_10)global.current_tower = instance_nearest(global.x,global.y,obj_green_tower)$(13_10)$(13_10)"
global.upgrade_current = "green"
global.current_tower = layer_instance_get_instance(self)
instance_create_layer(95,830,"Instances",obj_upgrades_list)
global.x = x
global.y = y
global.current_tower = instance_nearest(global.x,global.y,obj_green_tower)