/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 30D29B87
/// @DnDArgument : "code" "if global.units_spawned = global.units_to_spawn then$(13_10){$(13_10)global.currentlevel += 1	$(13_10)global.units_to_spawn = global.currentlevel + 10$(13_10)global.units_spawned = 0$(13_10)alarm_set(0,-1)$(13_10)instance_create_layer(1600,900,"Instances",obj_next_level)$(13_10)global.gold += global.currentlevel + 2$(13_10)audio_play_sound(snd_coin_pickup,1,0)$(13_10)}"
if global.units_spawned = global.units_to_spawn then
{
global.currentlevel += 1	
global.units_to_spawn = global.currentlevel + 10
global.units_spawned = 0
alarm_set(0,-1)
instance_create_layer(1600,900,"Instances",obj_next_level)
global.gold += global.currentlevel + 2
audio_play_sound(snd_coin_pickup,1,0)
}