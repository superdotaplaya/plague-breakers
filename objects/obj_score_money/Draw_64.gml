/// @DnDAction : YoYo Games.Drawing.Set_Color
/// @DnDVersion : 1
/// @DnDHash : 373B2837
/// @DnDArgument : "color" "$FF000000"
draw_set_colour($FF000000 & $ffffff);
var l373B2837_0=($FF000000 >> 24);
draw_set_alpha(l373B2837_0 / $ff);

/// @DnDAction : YoYo Games.Drawing.Draw_Value
/// @DnDVersion : 1
/// @DnDHash : 71DAD389
/// @DnDArgument : "x" "5"
/// @DnDArgument : "y" "10"
/// @DnDArgument : "caption" ""Gold: ""
/// @DnDArgument : "var" "global.gold"
draw_text(5, 10, string("Gold: ") + string(global.gold));

/// @DnDAction : YoYo Games.Drawing.Draw_Value
/// @DnDVersion : 1
/// @DnDHash : 587DF0A7
/// @DnDArgument : "x" "5"
/// @DnDArgument : "y" "30"
/// @DnDArgument : "caption" ""spawned: ""
/// @DnDArgument : "var" "global.currentlevel"
draw_text(5, 30, string("spawned: ") + string(global.currentlevel));

/// @DnDAction : YoYo Games.Drawing.Draw_Value
/// @DnDVersion : 1
/// @DnDHash : 778C5133
/// @DnDArgument : "x" "5"
/// @DnDArgument : "y" "45"
/// @DnDArgument : "caption" ""current tower: ""
/// @DnDArgument : "var" "global.current_tower"
draw_text(5, 45, string("current tower: ") + string(global.current_tower));