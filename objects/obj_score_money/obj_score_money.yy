{
    "id": "bd506929-690c-4d0b-bcc7-76f64dfd6fdb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_score_money",
    "eventList": [
        {
            "id": "66f5f86e-a3f5-439f-b2be-0c37efa3f6dc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "bd506929-690c-4d0b-bcc7-76f64dfd6fdb"
        },
        {
            "id": "771e3a57-68a5-4a48-89fe-eff98df8a759",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "bd506929-690c-4d0b-bcc7-76f64dfd6fdb"
        },
        {
            "id": "22178790-fa86-4d47-a26f-a7ad0bc1b560",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 27,
            "eventtype": 9,
            "m_owner": "bd506929-690c-4d0b-bcc7-76f64dfd6fdb"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}