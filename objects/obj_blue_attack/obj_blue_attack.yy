{
    "id": "a5618c59-9263-4868-885b-1ac700a1e9e9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_blue_attack",
    "eventList": [
        {
            "id": "b7654d05-7958-4d12-9a0f-05a712389eb7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a5618c59-9263-4868-885b-1ac700a1e9e9"
        },
        {
            "id": "a1d61a0d-c00d-4222-82d4-cc027dce823e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "a5618c59-9263-4868-885b-1ac700a1e9e9"
        },
        {
            "id": "6fff5e8f-4b2e-4903-b6e3-8828937be5d1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "17e75b6f-0a31-45d6-9c60-ca67cea49365",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "a5618c59-9263-4868-885b-1ac700a1e9e9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "5db1d80e-97e3-46da-9b27-cf9052d48797",
    "visible": true
}