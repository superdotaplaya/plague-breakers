/// @DnDAction : YoYo Games.Drawing.Set_Alpha
/// @DnDVersion : 1
/// @DnDHash : 27DD2801
draw_set_alpha(1);

/// @DnDAction : YoYo Games.Instances.Destroy_Instance
/// @DnDVersion : 1
/// @DnDHash : 35A9F906
/// @DnDApplyTo : 073fec17-ba80-4dec-a3c8-288b74078b2d
with(obj_tower_2_shop) instance_destroy();

/// @DnDAction : YoYo Games.Instances.Destroy_Instance
/// @DnDVersion : 1
/// @DnDHash : 78BFF165
/// @DnDApplyTo : 8aea4293-de50-4063-9f50-d75be1378031
with(obj_tower_1_shop) instance_destroy();

/// @DnDAction : YoYo Games.Instances.Destroy_Instance
/// @DnDVersion : 1
/// @DnDHash : 09046272
/// @DnDApplyTo : ff372800-7810-49bb-898f-a7defb5cc613
with(obj_tower_3_shop) instance_destroy();