/// @DnDAction : YoYo Games.Drawing.Draw_Self
/// @DnDVersion : 1
/// @DnDHash : 728355C9
draw_self();

/// @DnDAction : YoYo Games.Drawing.Set_Color
/// @DnDVersion : 1
/// @DnDHash : 41D6C52A
draw_set_colour($FFFFFFFF & $ffffff);
var l41D6C52A_0=($FFFFFFFF >> 24);
draw_set_alpha(l41D6C52A_0 / $ff);

/// @DnDAction : YoYo Games.Drawing.Draw_Value
/// @DnDVersion : 1
/// @DnDHash : 2421B8B2
/// @DnDArgument : "x" "-35"
/// @DnDArgument : "x_relative" "1"
/// @DnDArgument : "y" "-50"
/// @DnDArgument : "y_relative" "1"
/// @DnDArgument : "caption" ""Cost: ""
/// @DnDArgument : "var" "cost"
draw_text(x + -35, y + -50, string("Cost: ") + string(cost));

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 04B24F30
/// @DnDArgument : "var" "hover"
/// @DnDArgument : "value" "true"
if(hover == true)
{
	/// @DnDAction : YoYo Games.Drawing.Set_Alpha
	/// @DnDVersion : 1
	/// @DnDHash : 1776B13F
	/// @DnDParent : 04B24F30
	/// @DnDArgument : "alpha" ".5"
	draw_set_alpha(.5);

	/// @DnDAction : YoYo Games.Drawing.Draw_Gradient_Ellipse
	/// @DnDVersion : 1
	/// @DnDHash : 39F350C4
	/// @DnDParent : 04B24F30
	/// @DnDArgument : "x1" "global.x - 160"
	/// @DnDArgument : "y1" "global.y - 160"
	/// @DnDArgument : "x2" "global.x + 160"
	/// @DnDArgument : "y2" "global.y + 160"
	/// @DnDArgument : "col1" "$FFFF2007"
	/// @DnDArgument : "col2" "$FFFFA256"
	/// @DnDArgument : "fill" "1"
	draw_ellipse_colour(global.x - 160, global.y - 160, global.x + 160, global.y + 160, $FFFF2007 & $FFFFFF, $FFFFA256 & $FFFFFF, 0);
}