{
    "id": "ff372800-7810-49bb-898f-a7defb5cc613",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_tower_3_shop",
    "eventList": [
        {
            "id": "328b96a9-8afd-4c17-96b4-9d8a717add78",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "ff372800-7810-49bb-898f-a7defb5cc613"
        },
        {
            "id": "dda0d645-b565-4433-b3c0-3d3307ae1def",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "ff372800-7810-49bb-898f-a7defb5cc613"
        },
        {
            "id": "f32292b3-4949-4791-b0e2-c73699551930",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "ff372800-7810-49bb-898f-a7defb5cc613"
        },
        {
            "id": "97d2e011-f07e-461f-882f-082cb3e29b9e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "ff372800-7810-49bb-898f-a7defb5cc613"
        },
        {
            "id": "4d040c99-1381-4a52-ba57-8055cd79a404",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "ff372800-7810-49bb-898f-a7defb5cc613"
        },
        {
            "id": "dee6714f-3e72-437d-b532-5f7d342c6ad9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ff372800-7810-49bb-898f-a7defb5cc613"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "72079f4f-9558-4696-8b39-21e6a15d0714",
    "visible": true
}