{
    "id": "4343e1d3-0e54-446d-a179-4d5eb21f72b8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_green_attack",
    "eventList": [
        {
            "id": "a0b661c9-0024-4daf-91df-2e9202c64c80",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4343e1d3-0e54-446d-a179-4d5eb21f72b8"
        },
        {
            "id": "36a71e69-7e95-440c-b748-eb0b6c042b10",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "4343e1d3-0e54-446d-a179-4d5eb21f72b8"
        },
        {
            "id": "33b2835b-2bcf-4458-b221-f66338c76363",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "17e75b6f-0a31-45d6-9c60-ca67cea49365",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "4343e1d3-0e54-446d-a179-4d5eb21f72b8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "253138d9-82ea-4fef-9b55-02494aead4bd",
    "visible": true
}