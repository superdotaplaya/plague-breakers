{
    "id": "937a7c44-9bb2-4462-be16-0706945e428a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_red_attack_sub1",
    "eventList": [
        {
            "id": "1f754609-a6f6-450f-b343-1784b4416b5d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "937a7c44-9bb2-4462-be16-0706945e428a"
        },
        {
            "id": "64dd8fbd-bf27-488f-aaee-4cdb2c64c721",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "937a7c44-9bb2-4462-be16-0706945e428a"
        },
        {
            "id": "ef40fd0d-6f7a-4b1e-ba1c-517e4d26c3ce",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "17e75b6f-0a31-45d6-9c60-ca67cea49365",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "937a7c44-9bb2-4462-be16-0706945e428a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ad00aabc-3afc-4848-8f18-53f3606a4cc4",
    "visible": true
}