/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 55D6F4FD
/// @DnDArgument : "var" "global.gold"
/// @DnDArgument : "op" "4"
/// @DnDArgument : "value" "cost"
if(global.gold >= cost)
{
	/// @DnDAction : YoYo Games.Common.Set_Global
	/// @DnDVersion : 1
	/// @DnDHash : 1C088A1B
	/// @DnDParent : 55D6F4FD
	/// @DnDArgument : "value" "-cost"
	/// @DnDArgument : "value_relative" "1"
	/// @DnDArgument : "var" "gold"
	global.gold += -cost;

	/// @DnDAction : YoYo Games.Instances.Destroy_At_Position
	/// @DnDVersion : 1
	/// @DnDHash : 151771E7
	/// @DnDParent : 55D6F4FD
	/// @DnDArgument : "xpos" "global.x"
	/// @DnDArgument : "ypos" "global.y"
	position_destroy(global.x, global.y);

	/// @DnDAction : YoYo Games.Instances.Create_Instance
	/// @DnDVersion : 1
	/// @DnDHash : 0180A620
	/// @DnDParent : 55D6F4FD
	/// @DnDArgument : "xpos" "global.x"
	/// @DnDArgument : "ypos" "global.y"
	/// @DnDArgument : "objectid" "obj_orange_tower"
	/// @DnDSaveInfo : "objectid" "e0d5d9b9-11f4-496a-bceb-35cc321d3fc8"
	instance_create_layer(global.x, global.y, "Instances", obj_orange_tower);

	/// @DnDAction : YoYo Games.Instances.Destroy_Instance
	/// @DnDVersion : 1
	/// @DnDHash : 05DE1BDC
	/// @DnDApplyTo : 073fec17-ba80-4dec-a3c8-288b74078b2d
	/// @DnDParent : 55D6F4FD
	with(obj_tower_2_shop) instance_destroy();

	/// @DnDAction : YoYo Games.Instances.Destroy_Instance
	/// @DnDVersion : 1
	/// @DnDHash : 25EEA9B5
	/// @DnDApplyTo : 8aea4293-de50-4063-9f50-d75be1378031
	/// @DnDParent : 55D6F4FD
	with(obj_tower_1_shop) instance_destroy();

	/// @DnDAction : YoYo Games.Instances.Destroy_Instance
	/// @DnDVersion : 1
	/// @DnDHash : 6E110DA2
	/// @DnDApplyTo : ff372800-7810-49bb-898f-a7defb5cc613
	/// @DnDParent : 55D6F4FD
	with(obj_tower_3_shop) instance_destroy();

	/// @DnDAction : YoYo Games.Instances.Destroy_Instance
	/// @DnDVersion : 1
	/// @DnDHash : 3424048C
	/// @DnDParent : 55D6F4FD
	instance_destroy();

	/// @DnDAction : YoYo Games.Instances.Destroy_Instance
	/// @DnDVersion : 1
	/// @DnDHash : 067CFDFD
	/// @DnDApplyTo : 16d11c02-1a7d-4f1e-8cf3-b39df52c2b8b
	/// @DnDParent : 55D6F4FD
	with(obj_tower_5_shop) instance_destroy();

	/// @DnDAction : YoYo Games.Instances.Set_Alarm
	/// @DnDVersion : 1
	/// @DnDHash : 401B7A00
	/// @DnDParent : 55D6F4FD
	/// @DnDArgument : "steps" "20"
	alarm_set(0, 20);
}