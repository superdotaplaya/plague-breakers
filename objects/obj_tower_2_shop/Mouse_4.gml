/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 55D6F4FD
/// @DnDArgument : "var" "global.gold"
/// @DnDArgument : "op" "4"
/// @DnDArgument : "value" "cost"
if(global.gold >= cost)
{
	/// @DnDAction : YoYo Games.Common.Set_Global
	/// @DnDVersion : 1
	/// @DnDHash : 1C088A1B
	/// @DnDParent : 55D6F4FD
	/// @DnDArgument : "value" "-cost"
	/// @DnDArgument : "value_relative" "1"
	/// @DnDArgument : "var" "gold"
	global.gold += -cost;

	/// @DnDAction : YoYo Games.Instances.Destroy_At_Position
	/// @DnDVersion : 1
	/// @DnDHash : 151771E7
	/// @DnDParent : 55D6F4FD
	/// @DnDArgument : "xpos" "global.x"
	/// @DnDArgument : "ypos" "global.y"
	position_destroy(global.x, global.y);

	/// @DnDAction : YoYo Games.Instances.Create_Instance
	/// @DnDVersion : 1
	/// @DnDHash : 0180A620
	/// @DnDParent : 55D6F4FD
	/// @DnDArgument : "xpos" "global.x"
	/// @DnDArgument : "ypos" "global.y"
	/// @DnDArgument : "objectid" "obj_red_tower"
	/// @DnDSaveInfo : "objectid" "baea24cd-ae3b-413e-87c2-8850a5377416"
	instance_create_layer(global.x, global.y, "Instances", obj_red_tower);

	/// @DnDAction : YoYo Games.Drawing.Set_Alpha
	/// @DnDVersion : 1
	/// @DnDHash : 19FCD6C0
	/// @DnDParent : 55D6F4FD
	draw_set_alpha(1);

	/// @DnDAction : YoYo Games.Instances.Destroy_Instance
	/// @DnDVersion : 1
	/// @DnDHash : 05DE1BDC
	/// @DnDApplyTo : 073fec17-ba80-4dec-a3c8-288b74078b2d
	/// @DnDParent : 55D6F4FD
	with(obj_tower_2_shop) instance_destroy();

	/// @DnDAction : YoYo Games.Instances.Destroy_Instance
	/// @DnDVersion : 1
	/// @DnDHash : 25EEA9B5
	/// @DnDApplyTo : 8aea4293-de50-4063-9f50-d75be1378031
	/// @DnDParent : 55D6F4FD
	with(obj_tower_1_shop) instance_destroy();

	/// @DnDAction : YoYo Games.Instances.Destroy_Instance
	/// @DnDVersion : 1
	/// @DnDHash : 1CA51B70
	/// @DnDApplyTo : ff372800-7810-49bb-898f-a7defb5cc613
	/// @DnDParent : 55D6F4FD
	with(obj_tower_3_shop) instance_destroy();

	/// @DnDAction : YoYo Games.Instances.Destroy_Instance
	/// @DnDVersion : 1
	/// @DnDHash : 06A51664
	/// @DnDApplyTo : 43c1b6e1-4e5c-402d-9b7f-54483b5eb297
	/// @DnDParent : 55D6F4FD
	with(obj_tower_4_shop) instance_destroy();

	/// @DnDAction : YoYo Games.Instances.Destroy_Instance
	/// @DnDVersion : 1
	/// @DnDHash : 48A8C41B
	/// @DnDApplyTo : 16d11c02-1a7d-4f1e-8cf3-b39df52c2b8b
	/// @DnDParent : 55D6F4FD
	with(obj_tower_5_shop) instance_destroy();
}