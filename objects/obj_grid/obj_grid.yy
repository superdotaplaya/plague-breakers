{
    "id": "7b4a9b34-6545-4240-869f-e66525bd0fcc",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_grid",
    "eventList": [
        {
            "id": "101e5554-e1ee-47e4-abba-964a2981bbeb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "7b4a9b34-6545-4240-869f-e66525bd0fcc"
        },
        {
            "id": "5ac97f4c-a451-4d2f-b60f-9b893902ca06",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "7b4a9b34-6545-4240-869f-e66525bd0fcc"
        },
        {
            "id": "0b8e1916-8e4b-4306-bea2-2d6f28004fdb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7b4a9b34-6545-4240-869f-e66525bd0fcc"
        },
        {
            "id": "f0728237-40f8-4866-b875-99fe4c5fb6ef",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "7b4a9b34-6545-4240-869f-e66525bd0fcc"
        },
        {
            "id": "c2f2644a-9f30-45db-aa2a-63f9f6b3c0d5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "7b4a9b34-6545-4240-869f-e66525bd0fcc"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8024de4a-5f56-4836-9a65-fd17fd8bed4b",
    "visible": true
}