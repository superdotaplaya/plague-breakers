/// @DnDAction : YoYo Games.Instances.Create_Instance
/// @DnDVersion : 1
/// @DnDHash : 68EB2C21
/// @DnDArgument : "objectid" "obj_shop_menu"
/// @DnDSaveInfo : "objectid" "37f57748-c862-493a-bbbd-c8ffdf8f8d60"
instance_create_layer(0, 0, "Instances", obj_shop_menu);

/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 00F70E6D
/// @DnDArgument : "code" "global.x = x$(13_10)global.y = y$(13_10)global.activecell = instance_id$(13_10)$(13_10)"
global.x = x
global.y = y
global.activecell = instance_id

/// @DnDAction : YoYo Games.Instances.Color_Sprite
/// @DnDVersion : 1
/// @DnDHash : 34358DA2
/// @DnDApplyTo : 7b4a9b34-6545-4240-869f-e66525bd0fcc
with(obj_grid) {
image_blend = $FFFFFFFF & $ffffff;
image_alpha = ($FFFFFFFF >> 24) / $ff;
}

/// @DnDAction : YoYo Games.Instances.Color_Sprite
/// @DnDVersion : 1
/// @DnDHash : 3A0C9A1E
/// @DnDArgument : "colour" "$FF02FF06"
image_blend = $FF02FF06 & $ffffff;
image_alpha = ($FF02FF06 >> 24) / $ff;