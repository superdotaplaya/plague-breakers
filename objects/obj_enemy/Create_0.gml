/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 7D584960
/// @DnDArgument : "code" "if global.currentlevel <= 5 then$(13_10){$(13_10)type = irandom(1)	$(13_10)}$(13_10)if global.currentlevel > 5 and global.currentlevel < 15 then$(13_10){$(13_10)type = irandom_range(2,4)	$(13_10)}$(13_10)if global.currentlevel >= 15 then$(13_10){$(13_10)type = irandom(6)$(13_10)}$(13_10)$(13_10)$(13_10)$(13_10)if type = 1 then$(13_10){$(13_10)hp = 5 + (global.currentlevel * .5)	$(13_10)spd = 1.5$(13_10)stolen = 0$(13_10)}$(13_10)if type = 0 then$(13_10){$(13_10)hp = 5 + (global.currentlevel * .5)	$(13_10)spd = 1.5$(13_10)stolen = 0$(13_10)}$(13_10)if type = 2 then$(13_10){$(13_10)hp = 10	+ (global.currentlevel * .3)	$(13_10)spd = 1.5$(13_10)stolen = 0$(13_10)}$(13_10)if type = 3 then$(13_10){$(13_10)hp = 15 + (global.currentlevel * .4)	$(13_10)spd = 1$(13_10)stolen = 0$(13_10)}$(13_10)if type = 4 then$(13_10){$(13_10)hp = 15 + (global.currentlevel * .4)	$(13_10)spd = 1$(13_10)stolen = 0$(13_10)}$(13_10)if type = 5 then$(13_10){$(13_10)hp = 20	 + (global.currentlevel * .4)	$(13_10)spd = 1$(13_10)stolen = 0$(13_10)}$(13_10)if type = 6 then$(13_10){$(13_10)hp = 20	+ (global.currentlevel * .4)	$(13_10)spd = 1$(13_10)stolen = 0$(13_10)}"
if global.currentlevel <= 5 then
{
type = irandom(1)	
}
if global.currentlevel > 5 and global.currentlevel < 15 then
{
type = irandom_range(2,4)	
}
if global.currentlevel >= 15 then
{
type = irandom(6)
}



if type = 1 then
{
hp = 5 + (global.currentlevel * .5)	
spd = 1.5
stolen = 0
}
if type = 0 then
{
hp = 5 + (global.currentlevel * .5)	
spd = 1.5
stolen = 0
}
if type = 2 then
{
hp = 10	+ (global.currentlevel * .3)	
spd = 1.5
stolen = 0
}
if type = 3 then
{
hp = 15 + (global.currentlevel * .4)	
spd = 1
stolen = 0
}
if type = 4 then
{
hp = 15 + (global.currentlevel * .4)	
spd = 1
stolen = 0
}
if type = 5 then
{
hp = 20	 + (global.currentlevel * .4)	
spd = 1
stolen = 0
}
if type = 6 then
{
hp = 20	+ (global.currentlevel * .4)	
spd = 1
stolen = 0
}

/// @DnDAction : YoYo Games.Paths.Start_Path
/// @DnDVersion : 1.1
/// @DnDHash : 68E5A83F
/// @DnDArgument : "path" "forest"
/// @DnDArgument : "speed" "spd"
/// @DnDArgument : "relative" "true"
/// @DnDSaveInfo : "path" "6ce8db01-5fd6-4915-a783-e6afae5fee65"
path_start(forest, spd, path_action_stop, true);

/// @DnDAction : YoYo Games.Instances.Sprite_Scale
/// @DnDVersion : 1
/// @DnDHash : 640AFC1B
/// @DnDArgument : "xscale" ".5"
/// @DnDArgument : "yscale" ".5"
image_xscale = .5;
image_yscale = .5;

/// @DnDAction : YoYo Games.Common.Set_Global
/// @DnDVersion : 1
/// @DnDHash : 55A165A8
/// @DnDArgument : "value" " 5"
/// @DnDArgument : "var" "remaining_spawns"
global.remaining_spawns =  5;

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 0A988F5D
/// @DnDArgument : "var" "type"
if(type == 0)
{
	/// @DnDAction : YoYo Games.Instances.Set_Sprite
	/// @DnDVersion : 1
	/// @DnDHash : 16F506CB
	/// @DnDParent : 0A988F5D
	/// @DnDArgument : "spriteind" "spr_green_enemy"
	/// @DnDSaveInfo : "spriteind" "1795b121-2cd7-4e63-a7bf-3fc0cde4db8c"
	sprite_index = spr_green_enemy;
	image_index = 0;
}

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 6D890C18
/// @DnDArgument : "var" "type"
/// @DnDArgument : "value" "1"
if(type == 1)
{
	/// @DnDAction : YoYo Games.Instances.Set_Sprite
	/// @DnDVersion : 1
	/// @DnDHash : 036F1821
	/// @DnDParent : 6D890C18
	/// @DnDArgument : "spriteind" "spr_blue_enemy"
	/// @DnDSaveInfo : "spriteind" "bd7ec843-ded7-4fcc-99c6-e7a58292f42f"
	sprite_index = spr_blue_enemy;
	image_index = 0;
}

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 0B8E7D09
/// @DnDArgument : "var" "type"
/// @DnDArgument : "value" "2"
if(type == 2)
{
	/// @DnDAction : YoYo Games.Instances.Set_Sprite
	/// @DnDVersion : 1
	/// @DnDHash : 5107EF42
	/// @DnDParent : 0B8E7D09
	/// @DnDArgument : "spriteind" "spr_orange_enemy"
	/// @DnDSaveInfo : "spriteind" "54a4c327-f5eb-4d20-ae00-f9f3043399d1"
	sprite_index = spr_orange_enemy;
	image_index = 0;
}

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 5154F3AC
/// @DnDArgument : "var" "type"
/// @DnDArgument : "value" "3"
if(type == 3)
{
	/// @DnDAction : YoYo Games.Instances.Set_Sprite
	/// @DnDVersion : 1
	/// @DnDHash : 0795FDE3
	/// @DnDParent : 5154F3AC
	/// @DnDArgument : "spriteind" "spr_pink_enemy"
	/// @DnDSaveInfo : "spriteind" "fbc26842-b94e-445f-85cb-da759c3b0bef"
	sprite_index = spr_pink_enemy;
	image_index = 0;
}

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 1E05C7F3
/// @DnDArgument : "var" "type"
/// @DnDArgument : "value" "4"
if(type == 4)
{
	/// @DnDAction : YoYo Games.Instances.Set_Sprite
	/// @DnDVersion : 1
	/// @DnDHash : 33524A67
	/// @DnDParent : 1E05C7F3
	/// @DnDArgument : "spriteind" "spr_red_enemy"
	/// @DnDSaveInfo : "spriteind" "fb2ab5e1-6228-4de6-8807-874807409083"
	sprite_index = spr_red_enemy;
	image_index = 0;
}

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 5F7395F7
/// @DnDArgument : "var" "type"
/// @DnDArgument : "value" "5"
if(type == 5)
{
	/// @DnDAction : YoYo Games.Instances.Set_Sprite
	/// @DnDVersion : 1
	/// @DnDHash : 3005B47E
	/// @DnDParent : 5F7395F7
	/// @DnDArgument : "spriteind" "spr_white_enemy"
	/// @DnDSaveInfo : "spriteind" "d76a1cb6-60f9-4d7b-a963-046dc809a89a"
	sprite_index = spr_white_enemy;
	image_index = 0;
}

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 179754F6
/// @DnDArgument : "var" "type"
/// @DnDArgument : "value" "6"
if(type == 6)
{
	/// @DnDAction : YoYo Games.Instances.Set_Sprite
	/// @DnDVersion : 1
	/// @DnDHash : 1524F905
	/// @DnDParent : 179754F6
	/// @DnDArgument : "spriteind" "spr_yellow_enemy"
	/// @DnDSaveInfo : "spriteind" "fbd12d3e-0329-453f-9aa5-5ada9508f44f"
	sprite_index = spr_yellow_enemy;
	image_index = 0;
}