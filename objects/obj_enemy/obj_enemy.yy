{
    "id": "17e75b6f-0a31-45d6-9c60-ca67cea49365",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_enemy",
    "eventList": [
        {
            "id": "b37fe945-fb58-4c64-968c-0668aaf99525",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "17e75b6f-0a31-45d6-9c60-ca67cea49365"
        },
        {
            "id": "8cfc1f6f-e63c-469a-839e-1ec20c99f059",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "17e75b6f-0a31-45d6-9c60-ca67cea49365"
        },
        {
            "id": "387ba0ed-9914-41b6-bc90-374cfff90c80",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "17e75b6f-0a31-45d6-9c60-ca67cea49365",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "17e75b6f-0a31-45d6-9c60-ca67cea49365"
        },
        {
            "id": "2e90e7ea-750b-469e-b337-d918e1b2469a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "17e75b6f-0a31-45d6-9c60-ca67cea49365"
        },
        {
            "id": "f628a420-3771-4f8a-b863-cbc688c1d5bc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "4343e1d3-0e54-446d-a179-4d5eb21f72b8",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "17e75b6f-0a31-45d6-9c60-ca67cea49365"
        },
        {
            "id": "6755fbc4-c707-407c-b4de-9a8a3e1ca498",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "5a39ad46-1804-4ad1-951d-bd9239e0b8cd",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "17e75b6f-0a31-45d6-9c60-ca67cea49365"
        },
        {
            "id": "cf120d5f-f7cd-42aa-ba85-5fae7bd299a3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "e2b49a6d-6db2-4ec5-a186-b25158aff264",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "17e75b6f-0a31-45d6-9c60-ca67cea49365"
        },
        {
            "id": "afbdd560-c1a9-41c6-bc31-6ef2cd0b2e76",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "f9f14a6f-b432-4b2e-afd9-92d8085cc2fb",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "17e75b6f-0a31-45d6-9c60-ca67cea49365"
        },
        {
            "id": "09e4c043-528e-4334-97a0-86ded74e290a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "937a7c44-9bb2-4462-be16-0706945e428a",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "17e75b6f-0a31-45d6-9c60-ca67cea49365"
        },
        {
            "id": "e55cf0e2-b9eb-4c47-a77d-20475c2b76e4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "bac4d5f9-44f3-4434-aa73-829bda86ce59",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "17e75b6f-0a31-45d6-9c60-ca67cea49365"
        },
        {
            "id": "20f24a0a-66e0-4985-aa61-c5c06eee4598",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "a5618c59-9263-4868-885b-1ac700a1e9e9",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "17e75b6f-0a31-45d6-9c60-ca67cea49365"
        },
        {
            "id": "a1b07936-878a-4a49-9b74-53f83b950cb0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "113faadd-2ec4-468a-9d20-75913c9647da",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "17e75b6f-0a31-45d6-9c60-ca67cea49365"
        },
        {
            "id": "758e2b66-5758-446d-a4ec-04bba9337e19",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "721454be-8a92-444d-8eaf-873df8cbf18d",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "17e75b6f-0a31-45d6-9c60-ca67cea49365"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1795b121-2cd7-4e63-a7bf-3fc0cde4db8c",
    "visible": true
}