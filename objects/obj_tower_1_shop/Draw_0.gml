/// @DnDAction : YoYo Games.Drawing.Draw_Self
/// @DnDVersion : 1
/// @DnDHash : 728355C9
draw_self();

/// @DnDAction : YoYo Games.Drawing.Set_Color
/// @DnDVersion : 1
/// @DnDHash : 56DAE4C1
draw_set_colour($FFFFFFFF & $ffffff);
var l56DAE4C1_0=($FFFFFFFF >> 24);
draw_set_alpha(l56DAE4C1_0 / $ff);

/// @DnDAction : YoYo Games.Drawing.Draw_Value
/// @DnDVersion : 1
/// @DnDHash : 5D65EC72
/// @DnDArgument : "x" "-35"
/// @DnDArgument : "x_relative" "1"
/// @DnDArgument : "y" "-50"
/// @DnDArgument : "y_relative" "1"
/// @DnDArgument : "caption" ""Cost: ""
/// @DnDArgument : "var" "cost"
draw_text(x + -35, y + -50, string("Cost: ") + string(cost));

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 04B24F30
/// @DnDArgument : "var" "hover"
/// @DnDArgument : "value" "true"
if(hover == true)
{
	/// @DnDAction : YoYo Games.Drawing.Set_Alpha
	/// @DnDVersion : 1
	/// @DnDHash : 1776B13F
	/// @DnDParent : 04B24F30
	/// @DnDArgument : "alpha" ".5"
	draw_set_alpha(.5);

	/// @DnDAction : YoYo Games.Drawing.Draw_Gradient_Ellipse
	/// @DnDVersion : 1
	/// @DnDHash : 6FD972D4
	/// @DnDParent : 04B24F30
	/// @DnDArgument : "x1" "global.x -110"
	/// @DnDArgument : "y1" "global.y -110"
	/// @DnDArgument : "x2" "global.x + 110"
	/// @DnDArgument : "y2" "global.y + 110"
	/// @DnDArgument : "col1" "$FF0AFF0E"
	/// @DnDArgument : "col2" "$FF7FFFA3"
	/// @DnDArgument : "fill" "1"
	draw_ellipse_colour(global.x -110, global.y -110, global.x + 110, global.y + 110, $FF0AFF0E & $FFFFFF, $FF7FFFA3 & $FFFFFF, 0);
}