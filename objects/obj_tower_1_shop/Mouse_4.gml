/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 4B1286B8
/// @DnDArgument : "var" "global.gold"
/// @DnDArgument : "op" "4"
/// @DnDArgument : "value" "cost"
if(global.gold >= cost)
{
	/// @DnDAction : YoYo Games.Common.Set_Global
	/// @DnDVersion : 1
	/// @DnDHash : 5673953E
	/// @DnDParent : 4B1286B8
	/// @DnDArgument : "value" "-cost"
	/// @DnDArgument : "value_relative" "1"
	/// @DnDArgument : "var" "gold"
	global.gold += -cost;

	/// @DnDAction : YoYo Games.Instances.Destroy_At_Position
	/// @DnDVersion : 1
	/// @DnDHash : 20752A98
	/// @DnDParent : 4B1286B8
	/// @DnDArgument : "xpos" "global.x"
	/// @DnDArgument : "ypos" "global.y"
	position_destroy(global.x, global.y);

	/// @DnDAction : YoYo Games.Instances.Create_Instance
	/// @DnDVersion : 1
	/// @DnDHash : 7B76976E
	/// @DnDParent : 4B1286B8
	/// @DnDArgument : "xpos" "global.x"
	/// @DnDArgument : "ypos" "global.y"
	/// @DnDArgument : "objectid" "obj_green_tower"
	/// @DnDSaveInfo : "objectid" "cae81964-d4ac-454f-a5a2-c281915d634f"
	instance_create_layer(global.x, global.y, "Instances", obj_green_tower);

	/// @DnDAction : YoYo Games.Instances.Destroy_Instance
	/// @DnDVersion : 1
	/// @DnDHash : 32B9357A
	/// @DnDApplyTo : 073fec17-ba80-4dec-a3c8-288b74078b2d
	/// @DnDParent : 4B1286B8
	with(obj_tower_2_shop) instance_destroy();

	/// @DnDAction : YoYo Games.Instances.Destroy_Instance
	/// @DnDVersion : 1
	/// @DnDHash : 493E9526
	/// @DnDApplyTo : 8aea4293-de50-4063-9f50-d75be1378031
	/// @DnDParent : 4B1286B8
	with(obj_tower_1_shop) instance_destroy();

	/// @DnDAction : YoYo Games.Instances.Destroy_Instance
	/// @DnDVersion : 1
	/// @DnDHash : 7A5CE46C
	/// @DnDApplyTo : ff372800-7810-49bb-898f-a7defb5cc613
	/// @DnDParent : 4B1286B8
	with(obj_tower_3_shop) instance_destroy();

	/// @DnDAction : YoYo Games.Drawing.Set_Alpha
	/// @DnDVersion : 1
	/// @DnDHash : 3018B23F
	/// @DnDParent : 4B1286B8
	draw_set_alpha(1);

	/// @DnDAction : YoYo Games.Instances.Destroy_Instance
	/// @DnDVersion : 1
	/// @DnDHash : 62A27713
	/// @DnDApplyTo : 43c1b6e1-4e5c-402d-9b7f-54483b5eb297
	/// @DnDParent : 4B1286B8
	with(obj_tower_4_shop) instance_destroy();

	/// @DnDAction : YoYo Games.Instances.Destroy_Instance
	/// @DnDVersion : 1
	/// @DnDHash : 37A823D3
	/// @DnDApplyTo : 16d11c02-1a7d-4f1e-8cf3-b39df52c2b8b
	/// @DnDParent : 4B1286B8
	with(obj_tower_5_shop) instance_destroy();
}