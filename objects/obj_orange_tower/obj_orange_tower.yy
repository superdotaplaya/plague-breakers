{
    "id": "e0d5d9b9-11f4-496a-bceb-35cc321d3fc8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_orange_tower",
    "eventList": [
        {
            "id": "d6cd1d33-12cf-4a37-81ed-30244bd8c969",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "e0d5d9b9-11f4-496a-bceb-35cc321d3fc8"
        },
        {
            "id": "c40f9e2e-019d-4c2b-88f9-fca83776a903",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e0d5d9b9-11f4-496a-bceb-35cc321d3fc8"
        },
        {
            "id": "36891afd-fab6-4e4c-ad2a-79f9acb2b738",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "e0d5d9b9-11f4-496a-bceb-35cc321d3fc8"
        },
        {
            "id": "b7546cc5-8064-4747-89ac-e6febb167f13",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "e0d5d9b9-11f4-496a-bceb-35cc321d3fc8"
        },
        {
            "id": "718d7a29-936f-4537-91fe-d0c5466e5eee",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "e0d5d9b9-11f4-496a-bceb-35cc321d3fc8"
        },
        {
            "id": "41e108bb-d825-49f3-9808-8590bcb890ca",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e0d5d9b9-11f4-496a-bceb-35cc321d3fc8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c4e7204c-bd0b-46b6-b2a1-b15035274049",
    "visible": true
}