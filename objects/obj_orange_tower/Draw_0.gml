/// @DnDAction : YoYo Games.Drawing.Draw_Self
/// @DnDVersion : 1
/// @DnDHash : 73BF8011
draw_self();

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 4B458BDE
/// @DnDArgument : "var" "hover"
/// @DnDArgument : "value" "true"
if(hover == true)
{
	/// @DnDAction : YoYo Games.Drawing.Set_Alpha
	/// @DnDVersion : 1
	/// @DnDHash : 0BBFFABE
	/// @DnDParent : 4B458BDE
	/// @DnDArgument : "alpha" ".5"
	draw_set_alpha(.5);

	/// @DnDAction : YoYo Games.Drawing.Draw_Gradient_Ellipse
	/// @DnDVersion : 1
	/// @DnDHash : 277347EB
	/// @DnDParent : 4B458BDE
	/// @DnDArgument : "x1" "-80"
	/// @DnDArgument : "x1_relative" "1"
	/// @DnDArgument : "y1" "-80"
	/// @DnDArgument : "y1_relative" "1"
	/// @DnDArgument : "x2" "80"
	/// @DnDArgument : "x2_relative" "1"
	/// @DnDArgument : "y2" "80"
	/// @DnDArgument : "y2_relative" "1"
	/// @DnDArgument : "col1" "$FF0179C4"
	/// @DnDArgument : "col2" "$FF14C8FF"
	/// @DnDArgument : "fill" "1"
	draw_ellipse_colour(x + -80, y + -80, x + 80, y + 80, $FF0179C4 & $FFFFFF, $FF14C8FF & $FFFFFF, 0);
}