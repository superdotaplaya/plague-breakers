{
    "id": "721454be-8a92-444d-8eaf-873df8cbf18d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_gold",
    "eventList": [
        {
            "id": "f19d825a-8d8c-43ba-971f-351563f4efe9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "721454be-8a92-444d-8eaf-873df8cbf18d"
        },
        {
            "id": "53a1ef56-f3d2-4a30-8ddc-6bc2bcc99dc9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "721454be-8a92-444d-8eaf-873df8cbf18d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b1ac8a00-419f-4e13-b41d-288459b9a5ce",
    "visible": true
}