{
    "id": "16d11c02-1a7d-4f1e-8cf3-b39df52c2b8b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_tower_5_shop",
    "eventList": [
        {
            "id": "c8d2dd24-03b4-413d-9f80-ce1dc2e6360e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "16d11c02-1a7d-4f1e-8cf3-b39df52c2b8b"
        },
        {
            "id": "f9970217-8221-4d6b-bdc2-bfdb0e87dfa2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "16d11c02-1a7d-4f1e-8cf3-b39df52c2b8b"
        },
        {
            "id": "cb115f49-f39b-481e-aaaf-03010a205602",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "16d11c02-1a7d-4f1e-8cf3-b39df52c2b8b"
        },
        {
            "id": "d7820313-a29b-408c-8b67-b596ad106bf7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "16d11c02-1a7d-4f1e-8cf3-b39df52c2b8b"
        },
        {
            "id": "04c6a494-6ced-4ed3-8128-6c1e1e6b166c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "16d11c02-1a7d-4f1e-8cf3-b39df52c2b8b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c82849b0-f7ad-4a5d-aadd-977a6138ee59",
    "visible": true
}