{
    "id": "93dda09b-b386-4ff7-9efe-c2fd7ddb9a5c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_green_tower_dmgup1",
    "eventList": [
        {
            "id": "3e5fdf18-7dd0-42a6-b7c7-20cc58af8125",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "93dda09b-b386-4ff7-9efe-c2fd7ddb9a5c"
        },
        {
            "id": "c8822e1b-8768-4e9e-93d9-8e71ccf63513",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "93dda09b-b386-4ff7-9efe-c2fd7ddb9a5c"
        },
        {
            "id": "e42674b6-f81b-4963-b261-1f0383dc08ab",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "93dda09b-b386-4ff7-9efe-c2fd7ddb9a5c"
        },
        {
            "id": "453f173d-2425-4e0b-9da1-784833287c37",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "93dda09b-b386-4ff7-9efe-c2fd7ddb9a5c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ff17cdf8-f43a-45c6-8068-c71366134d73",
    "visible": true
}