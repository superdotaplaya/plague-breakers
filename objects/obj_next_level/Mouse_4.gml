/// @DnDAction : YoYo Games.Instances.Set_Alarm
/// @DnDVersion : 1
/// @DnDHash : 44EC48A3
/// @DnDApplyTo : ab303306-eb35-4318-8ad1-e68f47107931
/// @DnDArgument : "steps" "rate"
with(obj_enemy_spawner) {
alarm_set(0, rate);

}

/// @DnDAction : YoYo Games.Common.Execute_Code
/// @DnDVersion : 1
/// @DnDHash : 269926ED
/// @DnDArgument : "code" "instance_destroy()"
instance_destroy()