/// @DnDAction : YoYo Games.Drawing.Draw_Self
/// @DnDVersion : 1
/// @DnDHash : 73BF8011
draw_self();

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 4B458BDE
/// @DnDArgument : "var" "hover"
/// @DnDArgument : "value" "true"
if(hover == true)
{
	/// @DnDAction : YoYo Games.Drawing.Set_Alpha
	/// @DnDVersion : 1
	/// @DnDHash : 0BBFFABE
	/// @DnDParent : 4B458BDE
	/// @DnDArgument : "alpha" ".5"
	draw_set_alpha(.5);

	/// @DnDAction : YoYo Games.Drawing.Draw_Gradient_Ellipse
	/// @DnDVersion : 1
	/// @DnDHash : 277347EB
	/// @DnDParent : 4B458BDE
	/// @DnDArgument : "x1" "-160"
	/// @DnDArgument : "x1_relative" "1"
	/// @DnDArgument : "y1" "-160"
	/// @DnDArgument : "y1_relative" "1"
	/// @DnDArgument : "x2" "160"
	/// @DnDArgument : "x2_relative" "1"
	/// @DnDArgument : "y2" "160"
	/// @DnDArgument : "y2_relative" "1"
	/// @DnDArgument : "col1" "$FFFF2007"
	/// @DnDArgument : "col2" "$FFFFA256"
	/// @DnDArgument : "fill" "1"
	draw_ellipse_colour(x + -160, y + -160, x + 160, y + 160, $FFFF2007 & $FFFFFF, $FFFFA256 & $FFFFFF, 0);
}