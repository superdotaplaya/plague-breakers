{
    "id": "dea51724-d4e7-4bbe-9278-bfb7b5e195c2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_blue_tower",
    "eventList": [
        {
            "id": "2ca844d5-112d-4270-9429-93e9f4a4e137",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "dea51724-d4e7-4bbe-9278-bfb7b5e195c2"
        },
        {
            "id": "88f3406f-04f0-487e-aaef-12c4244ccc0c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "dea51724-d4e7-4bbe-9278-bfb7b5e195c2"
        },
        {
            "id": "519dda2b-ba62-4ff7-8cce-76050e03e897",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "dea51724-d4e7-4bbe-9278-bfb7b5e195c2"
        },
        {
            "id": "e2ef1f31-8778-4a7c-b082-8c6ddd061e67",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "dea51724-d4e7-4bbe-9278-bfb7b5e195c2"
        },
        {
            "id": "bd7dc8c2-3878-4a8b-98e4-a0a0b096cb38",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "dea51724-d4e7-4bbe-9278-bfb7b5e195c2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "72079f4f-9558-4696-8b39-21e6a15d0714",
    "visible": true
}