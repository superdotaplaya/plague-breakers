{
    "id": "113faadd-2ec4-468a-9d20-75913c9647da",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_yellow_attack",
    "eventList": [
        {
            "id": "0cb83db3-094a-40f9-9590-fe25918a6879",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "17e75b6f-0a31-45d6-9c60-ca67cea49365",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "113faadd-2ec4-468a-9d20-75913c9647da"
        },
        {
            "id": "1d977dc9-3f36-4a53-a678-68df5dfef48b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "113faadd-2ec4-468a-9d20-75913c9647da"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "db6a0503-0957-430e-bc35-3fa42a1d7f8c",
    "visible": true
}