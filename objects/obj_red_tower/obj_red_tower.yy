{
    "id": "baea24cd-ae3b-413e-87c2-8850a5377416",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_red_tower",
    "eventList": [
        {
            "id": "678cc9d0-5d0b-4765-b50e-30d497431d2d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "baea24cd-ae3b-413e-87c2-8850a5377416"
        },
        {
            "id": "a865011d-e814-402e-930f-78bef306f97f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "baea24cd-ae3b-413e-87c2-8850a5377416"
        },
        {
            "id": "3413ad27-94a9-40de-81f2-c298572c6683",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "baea24cd-ae3b-413e-87c2-8850a5377416"
        },
        {
            "id": "1a8b728a-1d73-4dfb-9ff8-88960efc8b75",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "baea24cd-ae3b-413e-87c2-8850a5377416"
        },
        {
            "id": "f77d5970-4de3-4294-acee-7e2829a50988",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "baea24cd-ae3b-413e-87c2-8850a5377416"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "afd048dc-5f11-4a66-a632-1d454265b957",
    "visible": true
}