{
    "id": "54a4c327-f5eb-4d20-ae00-f9f3043399d1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_orange_enemy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 83,
    "bbox_left": 48,
    "bbox_right": 91,
    "bbox_top": 46,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "385ad6fb-5c17-4d4a-a942-cf9ca894c875",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54a4c327-f5eb-4d20-ae00-f9f3043399d1",
            "compositeImage": {
                "id": "b679526a-15f9-4cab-abe5-01914ef76f01",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "385ad6fb-5c17-4d4a-a942-cf9ca894c875",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84a41d6f-e125-4037-bb96-56c400d7fa2b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "385ad6fb-5c17-4d4a-a942-cf9ca894c875",
                    "LayerId": "0fa2840c-ac76-497b-bed0-decb16a59195"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "0fa2840c-ac76-497b-bed0-decb16a59195",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "54a4c327-f5eb-4d20-ae00-f9f3043399d1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}