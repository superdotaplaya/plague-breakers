{
    "id": "f3f49e62-29bf-4389-aae4-b22d528fccb0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_orange_attack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 159,
    "bbox_left": 0,
    "bbox_right": 159,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "56e85ef6-af5c-4b3b-8491-69b76728ef1a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f3f49e62-29bf-4389-aae4-b22d528fccb0",
            "compositeImage": {
                "id": "705fe020-dfcc-47c4-b093-009da994c715",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56e85ef6-af5c-4b3b-8491-69b76728ef1a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eddf840b-7ecd-4459-8172-37c64d7632dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56e85ef6-af5c-4b3b-8491-69b76728ef1a",
                    "LayerId": "cfbbe0b1-0cc7-4f07-a3aa-96096107b563"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 160,
    "layers": [
        {
            "id": "cfbbe0b1-0cc7-4f07-a3aa-96096107b563",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f3f49e62-29bf-4389-aae4-b22d528fccb0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 160,
    "xorig": 80,
    "yorig": 80
}