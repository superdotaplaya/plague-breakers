{
    "id": "ff17cdf8-f43a-45c6-8068-c71366134d73",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_green_tower",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 48,
    "bbox_left": 14,
    "bbox_right": 48,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bc194546-f1fa-4478-bbed-5eb4b3c18ed6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ff17cdf8-f43a-45c6-8068-c71366134d73",
            "compositeImage": {
                "id": "865113d6-6d18-4995-b577-2aed9ce6a4d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc194546-f1fa-4478-bbed-5eb4b3c18ed6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3fe0fb6-5d9f-4eca-8ca6-339b8924af15",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc194546-f1fa-4478-bbed-5eb4b3c18ed6",
                    "LayerId": "8f055a13-0fd5-4f2d-ba7e-47a3a33e011c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "8f055a13-0fd5-4f2d-ba7e-47a3a33e011c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ff17cdf8-f43a-45c6-8068-c71366134d73",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}