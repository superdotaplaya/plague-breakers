{
    "id": "fbd12d3e-0329-453f-9aa5-5ada9508f44f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_yellow_enemy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 83,
    "bbox_left": 48,
    "bbox_right": 91,
    "bbox_top": 46,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "038400a9-70c5-4a5c-ab00-4038d7470a94",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fbd12d3e-0329-453f-9aa5-5ada9508f44f",
            "compositeImage": {
                "id": "0375cb7a-2e22-4217-a43d-7e9ade57a2f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "038400a9-70c5-4a5c-ab00-4038d7470a94",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5ca9c3c-254f-440d-98ae-bd12cbb5c67a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "038400a9-70c5-4a5c-ab00-4038d7470a94",
                    "LayerId": "03c53191-6d35-4eec-8326-125afe4a633e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "03c53191-6d35-4eec-8326-125afe4a633e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fbd12d3e-0329-453f-9aa5-5ada9508f44f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}