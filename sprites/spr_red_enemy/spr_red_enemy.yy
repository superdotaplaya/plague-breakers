{
    "id": "fb2ab5e1-6228-4de6-8807-874807409083",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_red_enemy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 83,
    "bbox_left": 48,
    "bbox_right": 91,
    "bbox_top": 46,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3fcf7e74-7739-4fc6-8b96-1b5856482602",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fb2ab5e1-6228-4de6-8807-874807409083",
            "compositeImage": {
                "id": "cbd9a57b-e339-4b8f-9a5b-e3bc7d9a72dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3fcf7e74-7739-4fc6-8b96-1b5856482602",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ae191a8-edb3-43ab-aade-2639ac600f16",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3fcf7e74-7739-4fc6-8b96-1b5856482602",
                    "LayerId": "03257269-9e7e-4bd5-b01a-88e70448627d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "03257269-9e7e-4bd5-b01a-88e70448627d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fb2ab5e1-6228-4de6-8807-874807409083",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}