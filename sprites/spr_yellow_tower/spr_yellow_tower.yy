{
    "id": "c82849b0-f7ad-4a5d-aadd-977a6138ee59",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_yellow_tower",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 48,
    "bbox_left": 14,
    "bbox_right": 48,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fa82bb75-11ad-4302-a20e-46e0e7209f99",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c82849b0-f7ad-4a5d-aadd-977a6138ee59",
            "compositeImage": {
                "id": "ce7d16f7-b113-4834-947e-d638909da7cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa82bb75-11ad-4302-a20e-46e0e7209f99",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34aaf222-77b1-42b9-b185-5e93dfa8ee1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa82bb75-11ad-4302-a20e-46e0e7209f99",
                    "LayerId": "43f821db-7a4e-45d5-864e-f8f5ec5dd17b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "43f821db-7a4e-45d5-864e-f8f5ec5dd17b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c82849b0-f7ad-4a5d-aadd-977a6138ee59",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}