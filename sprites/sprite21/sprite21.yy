{
    "id": "179e3ab1-4528-4d5b-91d0-ff3c8a2d4ef2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite21",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 119,
    "bbox_left": 0,
    "bbox_right": 1919,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "96834c0c-4922-4fb0-9f34-0c3854ae8e70",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "179e3ab1-4528-4d5b-91d0-ff3c8a2d4ef2",
            "compositeImage": {
                "id": "74e192e3-3cc7-42c4-a385-1d289caac4e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96834c0c-4922-4fb0-9f34-0c3854ae8e70",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11878085-f882-4e47-9bf8-1d77c80f4676",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96834c0c-4922-4fb0-9f34-0c3854ae8e70",
                    "LayerId": "8b80dce4-62f3-4a44-a939-50b4f9a95bbb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 120,
    "layers": [
        {
            "id": "8b80dce4-62f3-4a44-a939-50b4f9a95bbb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "179e3ab1-4528-4d5b-91d0-ff3c8a2d4ef2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1920,
    "xorig": 0,
    "yorig": 0
}