{
    "id": "72079f4f-9558-4696-8b39-21e6a15d0714",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_blue_tower",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 48,
    "bbox_left": 14,
    "bbox_right": 48,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2ab184a2-54b0-4c6a-9869-a00ef93e75a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "72079f4f-9558-4696-8b39-21e6a15d0714",
            "compositeImage": {
                "id": "83bb6386-f2e3-4944-a7ae-028705d0945f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ab184a2-54b0-4c6a-9869-a00ef93e75a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc988d17-c99f-4d3c-a399-c74f32e16913",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ab184a2-54b0-4c6a-9869-a00ef93e75a1",
                    "LayerId": "634caa2b-3031-45df-9d90-df038f371bce"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "634caa2b-3031-45df-9d90-df038f371bce",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "72079f4f-9558-4696-8b39-21e6a15d0714",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}