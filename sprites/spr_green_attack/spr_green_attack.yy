{
    "id": "253138d9-82ea-4fef-9b55-02494aead4bd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_green_attack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 25,
    "bbox_left": 6,
    "bbox_right": 24,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c9b96c90-7108-4cd4-aa3a-26d77df3e1a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "253138d9-82ea-4fef-9b55-02494aead4bd",
            "compositeImage": {
                "id": "5dac5573-16ab-416d-a8db-ef9d9fba5bce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9b96c90-7108-4cd4-aa3a-26d77df3e1a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fcfecab7-26ab-467c-bc9e-08f756520da8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9b96c90-7108-4cd4-aa3a-26d77df3e1a8",
                    "LayerId": "4cc2c31f-ab9e-443d-8e69-7eaef7f4e5be"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "4cc2c31f-ab9e-443d-8e69-7eaef7f4e5be",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "253138d9-82ea-4fef-9b55-02494aead4bd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}