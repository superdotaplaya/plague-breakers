{
    "id": "1795b121-2cd7-4e63-a7bf-3fc0cde4db8c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_green_enemy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 83,
    "bbox_left": 48,
    "bbox_right": 91,
    "bbox_top": 46,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "368b4b2a-3884-485b-87b5-f8da658e73c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1795b121-2cd7-4e63-a7bf-3fc0cde4db8c",
            "compositeImage": {
                "id": "fffd2be4-b2f4-471e-a99b-ed29135d66ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "368b4b2a-3884-485b-87b5-f8da658e73c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "431dcf9c-24a4-4d58-b71d-8ec09d8bd87b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "368b4b2a-3884-485b-87b5-f8da658e73c4",
                    "LayerId": "6f73a5e7-5dc6-4168-941d-37d22b42a510"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "6f73a5e7-5dc6-4168-941d-37d22b42a510",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1795b121-2cd7-4e63-a7bf-3fc0cde4db8c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}