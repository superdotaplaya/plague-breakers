{
    "id": "afd048dc-5f11-4a66-a632-1d454265b957",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_red_tower",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 48,
    "bbox_left": 14,
    "bbox_right": 48,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d19a19ab-022d-4591-af4e-167e17065444",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "afd048dc-5f11-4a66-a632-1d454265b957",
            "compositeImage": {
                "id": "0b888107-76d4-4e9e-b4a3-cf0c4bf43304",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d19a19ab-022d-4591-af4e-167e17065444",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6ef87cc-1008-41ba-8abe-ffbed2cde856",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d19a19ab-022d-4591-af4e-167e17065444",
                    "LayerId": "5c05bbf1-9fad-40eb-b231-b63bafff3b0e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "5c05bbf1-9fad-40eb-b231-b63bafff3b0e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "afd048dc-5f11-4a66-a632-1d454265b957",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}