{
    "id": "d76a1cb6-60f9-4d7b-a963-046dc809a89a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_white_enemy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 83,
    "bbox_left": 48,
    "bbox_right": 91,
    "bbox_top": 46,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3228d79f-6f51-4a26-b84e-8b39a9e6c935",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d76a1cb6-60f9-4d7b-a963-046dc809a89a",
            "compositeImage": {
                "id": "7b410864-435c-4495-b576-9f89e7323759",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3228d79f-6f51-4a26-b84e-8b39a9e6c935",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d18f7a1f-07b9-4ba6-8036-26f9e8302f89",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3228d79f-6f51-4a26-b84e-8b39a9e6c935",
                    "LayerId": "895480c1-ea65-431a-bc09-63ac853765e8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "895480c1-ea65-431a-bc09-63ac853765e8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d76a1cb6-60f9-4d7b-a963-046dc809a89a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}