{
    "id": "c4e7204c-bd0b-46b6-b2a1-b15035274049",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_orange_tower",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 48,
    "bbox_left": 14,
    "bbox_right": 48,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bbc1e09d-f973-4cc6-9f11-c711ce2cbb77",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4e7204c-bd0b-46b6-b2a1-b15035274049",
            "compositeImage": {
                "id": "c14c8193-4f13-435c-9568-76840d9a7a52",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bbc1e09d-f973-4cc6-9f11-c711ce2cbb77",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ecc04e5f-035d-4a5e-b7f0-3177c385c969",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbc1e09d-f973-4cc6-9f11-c711ce2cbb77",
                    "LayerId": "9e5c1f4d-b6cb-4947-b20c-002f59e653ce"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "9e5c1f4d-b6cb-4947-b20c-002f59e653ce",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c4e7204c-bd0b-46b6-b2a1-b15035274049",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}