{
    "id": "8024de4a-5f56-4836-9a65-fd17fd8bed4b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "grid",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f1ec876e-d0c3-450d-be2f-eb6a34e710ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8024de4a-5f56-4836-9a65-fd17fd8bed4b",
            "compositeImage": {
                "id": "9fe2cfef-59e8-4298-8825-274d401c5a07",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1ec876e-d0c3-450d-be2f-eb6a34e710ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb3ef30c-5513-4e60-b3f0-cac2958ec32d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1ec876e-d0c3-450d-be2f-eb6a34e710ec",
                    "LayerId": "4e71b4e3-d343-4710-8779-476208326d1e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "4e71b4e3-d343-4710-8779-476208326d1e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8024de4a-5f56-4836-9a65-fd17fd8bed4b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}