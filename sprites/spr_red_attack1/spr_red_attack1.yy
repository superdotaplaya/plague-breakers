{
    "id": "ad00aabc-3afc-4848-8f18-53f3606a4cc4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_red_attack1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 17,
    "bbox_left": 13,
    "bbox_right": 17,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4d77b6c0-362f-4d8f-9c63-732b39f600dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad00aabc-3afc-4848-8f18-53f3606a4cc4",
            "compositeImage": {
                "id": "7dabc528-d1e3-4ff5-986b-85744e304242",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d77b6c0-362f-4d8f-9c63-732b39f600dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f15be82e-93f3-4f3d-9954-e3b189780639",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d77b6c0-362f-4d8f-9c63-732b39f600dc",
                    "LayerId": "92cb3d17-0540-49c8-a9a6-3618fb7365fc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "92cb3d17-0540-49c8-a9a6-3618fb7365fc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ad00aabc-3afc-4848-8f18-53f3606a4cc4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}