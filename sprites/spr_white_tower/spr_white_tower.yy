{
    "id": "92de3f75-7cf1-48b2-ae14-43cffb5901c3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_white_tower",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 48,
    "bbox_left": 14,
    "bbox_right": 48,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d1e26f3b-9e5c-435c-884f-78cb9f3a0087",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "92de3f75-7cf1-48b2-ae14-43cffb5901c3",
            "compositeImage": {
                "id": "5ec7970b-a9a4-41e0-8b3f-0cc6b33aba92",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1e26f3b-9e5c-435c-884f-78cb9f3a0087",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2e5a48f-cd09-4329-af14-ee7695a2b04b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1e26f3b-9e5c-435c-884f-78cb9f3a0087",
                    "LayerId": "badf3d71-94be-404d-a5c1-86ead8818816"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "badf3d71-94be-404d-a5c1-86ead8818816",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "92de3f75-7cf1-48b2-ae14-43cffb5901c3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}