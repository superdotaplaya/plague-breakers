{
    "id": "b1ac8a00-419f-4e13-b41d-288459b9a5ce",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gold",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 26,
    "bbox_right": 40,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5e01667f-8ad7-4f45-a7f8-d321607e277a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1ac8a00-419f-4e13-b41d-288459b9a5ce",
            "compositeImage": {
                "id": "9fa8e48d-9529-4705-bb38-8290952ab478",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e01667f-8ad7-4f45-a7f8-d321607e277a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93ef1fb9-ee0b-4d88-9540-7666c68a8fef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e01667f-8ad7-4f45-a7f8-d321607e277a",
                    "LayerId": "18c852a5-9154-4bf3-9c47-39a359dcaf45"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "18c852a5-9154-4bf3-9c47-39a359dcaf45",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b1ac8a00-419f-4e13-b41d-288459b9a5ce",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}