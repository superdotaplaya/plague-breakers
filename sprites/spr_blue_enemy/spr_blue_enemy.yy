{
    "id": "bd7ec843-ded7-4fcc-99c6-e7a58292f42f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_blue_enemy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 83,
    "bbox_left": 48,
    "bbox_right": 91,
    "bbox_top": 46,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3a10d794-9fb2-43e2-8dde-c9bee768922a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bd7ec843-ded7-4fcc-99c6-e7a58292f42f",
            "compositeImage": {
                "id": "76bdd098-fa4e-409b-8c7a-e1447e83df75",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a10d794-9fb2-43e2-8dde-c9bee768922a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "282f5e3a-dde7-4324-9f0e-77ec1f8f201a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a10d794-9fb2-43e2-8dde-c9bee768922a",
                    "LayerId": "16d51834-6807-4ed8-abdf-25e2f167d06e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "16d51834-6807-4ed8-abdf-25e2f167d06e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bd7ec843-ded7-4fcc-99c6-e7a58292f42f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}