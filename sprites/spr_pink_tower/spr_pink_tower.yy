{
    "id": "92dc95d5-3f2b-416f-bc4b-84f7be27f74b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pink_tower",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 48,
    "bbox_left": 14,
    "bbox_right": 48,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e2d95135-80e4-41cd-adba-a3bdb578632d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "92dc95d5-3f2b-416f-bc4b-84f7be27f74b",
            "compositeImage": {
                "id": "42de4531-647d-446b-adcb-6fbba0976dd4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2d95135-80e4-41cd-adba-a3bdb578632d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "220cf8ae-2bd5-49b5-8a00-0762382b7f6f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2d95135-80e4-41cd-adba-a3bdb578632d",
                    "LayerId": "edac63ee-f82b-45a4-9c3d-785c6b5ebb76"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "edac63ee-f82b-45a4-9c3d-785c6b5ebb76",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "92dc95d5-3f2b-416f-bc4b-84f7be27f74b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}