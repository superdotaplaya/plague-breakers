{
    "id": "f8636494-beb1-4902-9739-ccc4fd8e31f0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_red_attack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 25,
    "bbox_left": 6,
    "bbox_right": 24,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3636f4e3-e25d-4681-90ff-c1e77fb43b60",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8636494-beb1-4902-9739-ccc4fd8e31f0",
            "compositeImage": {
                "id": "ae2535a9-2fff-4318-a007-18b87f4cf0af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3636f4e3-e25d-4681-90ff-c1e77fb43b60",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00bdf4b5-bd25-4ed4-89f9-8cd45512f76c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3636f4e3-e25d-4681-90ff-c1e77fb43b60",
                    "LayerId": "c19331bb-9936-47f0-a370-75040fbdd224"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "c19331bb-9936-47f0-a370-75040fbdd224",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f8636494-beb1-4902-9739-ccc4fd8e31f0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}