{
    "id": "0580e01f-b8c6-4177-9155-40e970c61dd9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_base_path",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 670,
    "bbox_left": 0,
    "bbox_right": 1919,
    "bbox_top": 39,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "da30f1c6-f29b-4502-8a48-dd1c2e22572d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0580e01f-b8c6-4177-9155-40e970c61dd9",
            "compositeImage": {
                "id": "a53a68de-c6e1-4a74-a18b-bfc318c37417",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da30f1c6-f29b-4502-8a48-dd1c2e22572d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e874156b-7e18-42d8-971d-7f47177b6743",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da30f1c6-f29b-4502-8a48-dd1c2e22572d",
                    "LayerId": "993db63e-ee44-4e7a-9401-160ad5521439"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 768,
    "layers": [
        {
            "id": "993db63e-ee44-4e7a-9401-160ad5521439",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0580e01f-b8c6-4177-9155-40e970c61dd9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1920,
    "xorig": 0,
    "yorig": 0
}