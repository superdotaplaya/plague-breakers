{
    "id": "5db1d80e-97e3-46da-9b27-cf9052d48797",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_blue_attack",
    "For3D": false,
    "HTile": true,
    "VTile": false,
    "bbox_bottom": 25,
    "bbox_left": 6,
    "bbox_right": 24,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ee284aa0-cf88-4c43-9a7f-54c6dfff483c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5db1d80e-97e3-46da-9b27-cf9052d48797",
            "compositeImage": {
                "id": "2cd1c9bb-343b-412c-84de-8aa4a3fd80f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee284aa0-cf88-4c43-9a7f-54c6dfff483c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "853cc4b7-ad72-4f1d-b1e4-63d8a1a9303d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee284aa0-cf88-4c43-9a7f-54c6dfff483c",
                    "LayerId": "ec2f8692-e986-4a23-b97e-d032f78a45f8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ec2f8692-e986-4a23-b97e-d032f78a45f8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5db1d80e-97e3-46da-9b27-cf9052d48797",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}