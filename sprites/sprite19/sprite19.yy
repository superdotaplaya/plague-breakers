{
    "id": "74c33128-bb20-466a-877f-9b3464e23886",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite19",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5cdb3a80-7539-48e6-85f2-684222803e81",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "74c33128-bb20-466a-877f-9b3464e23886",
            "compositeImage": {
                "id": "d778d822-de02-4224-9f89-6310a65722fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5cdb3a80-7539-48e6-85f2-684222803e81",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc0e5e6f-b838-4d38-9401-4daa081e6936",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5cdb3a80-7539-48e6-85f2-684222803e81",
                    "LayerId": "bc412746-73a4-4ffd-bc21-0e8e72863461"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "bc412746-73a4-4ffd-bc21-0e8e72863461",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "74c33128-bb20-466a-877f-9b3464e23886",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}