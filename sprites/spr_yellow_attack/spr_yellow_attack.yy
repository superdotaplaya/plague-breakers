{
    "id": "db6a0503-0957-430e-bc35-3fa42a1d7f8c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_yellow_attack",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 25,
    "bbox_left": 6,
    "bbox_right": 24,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "59a62de4-281f-4a0f-8823-ec517d203bb3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db6a0503-0957-430e-bc35-3fa42a1d7f8c",
            "compositeImage": {
                "id": "6b6036ee-7fe0-46da-86aa-81a0d033ccb7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59a62de4-281f-4a0f-8823-ec517d203bb3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a15a214-f9bd-42a1-8abd-af0ab4a46644",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59a62de4-281f-4a0f-8823-ec517d203bb3",
                    "LayerId": "81cd5cb5-5e1c-414c-b53a-9c5e0a528078"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "81cd5cb5-5e1c-414c-b53a-9c5e0a528078",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "db6a0503-0957-430e-bc35-3fa42a1d7f8c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}