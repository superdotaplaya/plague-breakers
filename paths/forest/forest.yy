{
    "id": "6ce8db01-5fd6-4915-a783-e6afae5fee65",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "forest",
    "closed": false,
    "hsnap": 0,
    "kind": 0,
    "points": [
        {
            "id": "1a78526b-5c73-4537-a9f8-c072569efb90",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": -64,
            "y": 224,
            "speed": 100
        },
        {
            "id": "14133200-0baf-4201-b44d-9d2999d4e521",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 881,
            "y": 62,
            "speed": 100
        },
        {
            "id": "6ca016aa-b884-49b3-bd21-32601bc9780f",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 695,
            "y": 221,
            "speed": 100
        },
        {
            "id": "b7e5c6f6-4100-4513-91b5-489a1f024f40",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 566,
            "y": 348,
            "speed": 100
        },
        {
            "id": "b82ced90-3101-42ec-a534-d58612dbbe8f",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 340,
            "y": 581,
            "speed": 100
        },
        {
            "id": "51b2e7cb-2699-4371-af99-f4402bbeac9b",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1145,
            "y": 441,
            "speed": 100
        },
        {
            "id": "b99bf398-d1ea-48bd-8e9f-00c13439f7de",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 344.824371,
            "y": 282.93222,
            "speed": 100
        },
        {
            "id": "08db9289-ea3c-4515-b43c-664d4b7ac471",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 187.1972,
            "y": 416.830536,
            "speed": 100
        },
        {
            "id": "b6dbf68b-45f5-44a3-b1f7-bbafdfef1129",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 263.43457,
            "y": 450.728821,
            "speed": 100
        },
        {
            "id": "92693b57-2b00-40e7-a994-6dcf01123a80",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1243.12952,
            "y": 654.118652,
            "speed": 100
        },
        {
            "id": "01bf072c-c627-4f0f-9832-c0180be376ae",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1370.48547,
            "y": 530.5085,
            "speed": 100
        },
        {
            "id": "0dcfc825-d096-492d-acdd-a1fcbb34a22c",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1524.70581,
            "y": 250.2034,
            "speed": 100
        },
        {
            "id": "2b9ccf90-2e59-417d-9dcb-f7b9445f8a87",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1592.34985,
            "y": 243.457642,
            "speed": 100
        },
        {
            "id": "68d48976-e74f-4a5d-8735-b0b410e99379",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 1939.87524,
            "y": 277.847473,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 0
}